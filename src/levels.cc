#include "levels.hpp"
#include "lift.hpp"
#include "platform.hpp"
#include "laserFly.hpp"

Level1::Level1() {

}
void Level1::load(std::function<void(float)>&& progress) {
    if(mLoaded){
        progress(1);
        return;
    }
    progress(0);
    SoundContainer::getInstance().loadSoundBuffer("ak47", "assets/sounds/ak47.wav");
    progress(0.1);
    SingleStringSprite::imageContainer::getInstance().loadSingleImage("platform","assets/images/test.png");
    progress(0.2);
    SingleStringSprite::imageContainer::getInstance().loadSprite("player",{
            {"assets/player/jump.png",                           "jump",                     6 ,5},
            {"assets/player/belly_sliding_and_shot.png",     "belly_sliding_and_shot",   3 ,3},
            {"assets/player/belly_sliding.png",              "belly_sliding",            3 ,3},
            {"assets/player/belly_sliding_start.png",            "belly_sliding_start",      6 ,5},
            {"assets/player/bullet_fly.png",                 "bullet_fly",               3 ,3},
            {"assets/player/bullet_impact.png",              "bullet_impact",            3 ,3},
            {"assets/player/bullet_shell_drop_long.png",     "bullet_shell_drop_long",   6 ,5},
            {"assets/player/bullet_shell_drop_short.png",        "bullet_shell_drop_short",  5 ,4},
            {"assets/player/crouch_idle.png",                    "crouch_idle",              8 ,6},
            {"assets/player/crouch_reload.png",              "crouch_reload",            8 ,6},
            {"assets/player/crouch_shot.png",                    "crouch_shot",              3 ,3},
            {"assets/player/crouch_start.png",                   "crouch_start",             5 ,3},
            {"assets/player/death_bat.png",                  "death_bat",                6 ,6},
            {"assets/player/death.png",                      "death",                    6 ,5},
            {"assets/player/hurt_bat.png",                       "hurt_bat",                 3 ,3},
            {"assets/player/hurt.png",                           "hurt",                     3 ,3},
            {"assets/player/idle_aim_transition.png",            "idle_aim_transition",      6 ,5},
            {"assets/player/idle_bat_attack.png",                "idle_bat_attack",          4 ,3},
            {"assets/player/idle_bat.png",                       "idle_bat",                 6 ,5},
            {"assets/player/idle.png",                           "idle",                     6 ,6},
            {"assets/player/jump_bat_attack.png",                "jump_bat_attack",          4 ,3},
            {"assets/player/jump_bat_fall.png",              "jump_bat_fall",            3 ,3},
            {"assets/player/jump_bat.png",                       "jump_bat",                 6 ,5},
            {"assets/player/jump_fall.png",                  "jump_fall",                3 ,3},
            {"assets/player/jump_shot.png",                  "jump_shot",                3 ,3},
            {"assets/player/reload.png",                     "reload",                   6 ,6},
            {"assets/player/run_aim_transition.png",         "run_aim_transition",       6 ,5},
            {"assets/player/run_bat_attack.png",             "run_bat_attack",           6 ,5},
            {"assets/player/run_bat.png",                        "run_bat",                  6 ,3},
            {"assets/player/run.png",                            "run",                      6 ,3},
            {"assets/player/run_reload.png",                 "run_reload",               8 ,6},
            {"assets/player/run_shot_down.png",              "run_shot_down",            6 ,5},
            {"assets/player/run_shot.png",                       "run_shot",                 6 ,3},
            {"assets/player/run_shot_up.png",                    "run_shot_up",              6 ,5},
            {"assets/player/shot_diagonal_down.png",         "shot_diagonal_down",       3 ,3},
            {"assets/player/shot_diagonal_up.png",               "shot_diagonal_up",         3 ,3},
            {"assets/player/shot.png",                           "shot",                     3 ,3},
            {"assets/player/shot_up.png",                        "shot_up",                  3 ,3},
            {"assets/player/stand_up.png",                       "stand_up",                 5 ,3}
        });
    progress(0.7);
    SingleStringSprite::imageContainer::getInstance().loadSprite("laserFly",{
        {"assets/laser-flea/death.png",                       "death",6 , 4 },
        {"assets/laser-flea/hurt.png",                        "hurt", 7, 1 },
        {"assets/laser-flea/idle_change_direction.png",       "idle_change_direction", 14,7 },
        {"assets/laser-flea/idle_look_up.png",                "idle_look_up", 8, 6 },
        {"assets/laser-flea/idle.png",                        "idle", 8, 6 },
        {"assets/laser-flea/jump.png",                        "jump", 6, 4 },
        {"assets/laser-flea/laser_attack_forward.png",        "laser_attack_forward", 10, 6 },
        {"assets/laser-flea/laser_attack_up.png",         "laser_attack_up",10 , 6 },
        {"assets/laser-flea/lasser_swept.png",                "lasser_swept",11 , 11 },
        {"assets/laser-flea/walk_change_direction.png",       "walk_change_direction", 5, 4 },
        {"assets/laser-flea/walk_look up.png",                "walk_look_up", 5, 4},
        {"assets/laser-flea/walk.png",                        "walk", 5, 4 }
    });
    SingleStringSprite::imageContainer::getInstance().loadSingleImage("level1_background", "assets/images/level1_background.jpg");
    auto texture = SingleStringSprite::imageContainer::getInstance().getSingleTexture("level1_background");
    texture->setRepeated(true);
    mBackground.setTexture(*texture);
    mBackground.setTextureRect(sf::IntRect{ -200, -200, 30000, 5000});
    progress(0.9);
    SoundContainer::getInstance().loadMusic("action", "assets/sounds/action.wav");
    SoundContainer::getInstance().loadSoundBuffer("baseball", "assets/sounds/baseball.wav");
    mActionMusic = SoundContainer::getInstance().getMusic("action");
    mActionMusic->setVolume(60);
    mActionMusic->setLoop(true);
    SingleStringSprite::imageContainer::getInstance().loadSingleImage("laser", "assets/laser-flea/laser.png");
    progress(1);
    mLoaded = true;
}
void Level1::operator()(Map& map) {
    map.printf(
            "############################################################################################################\n"
            "#                                                     P                                 F                  #\n"
            "#     P                                  F    LLLLLLLLLLLLLLLLL   F           F    LLLLLLLLLLLLLLLLLLL     #\n"
            "#  ###########################################                 ####################                        #\n"
            "#                                                                       P                                  #\n"
            "#                                                               LLLLLLLLLLLLLLLLLLL                        #\n"
            "#                                                                                                          #\n"
            "#                                 F              F                                                         #\n"
            "#          F            ##################################                               F         F       #\n"
            "#    LLLLLLLLLLLL                                                                     ###################  #\n"
            "#                                                                                                          #\n"
            "#                           F                                            P                                 #\n"
            "#                  ##################         F                       #####                        F       #\n"
            "#                                    ####################                                ##############    #\n"
            "#                                                                                                          #\n"
            "#                                                                                                          #\n"
            "#    P              F                   F                      F                     F                     #\n"
            "############################################################################################################\n",
            sf::Vector2u{50, 50}, 
            PlatformFactory( SingleStringSprite::imageContainer::getInstance().getSingleTexture("platform")), 
            LiftFactory(200, 1, 'L',  SingleStringSprite::imageContainer::getInstance().getSingleTexture("platform")),
            LaserFlyFactory(),PlayerSpawns());
    map.setBackground(mBackground);
    map.addPlayerScript(
        [](const Map&, const std::unique_ptr<Player>& player){ return player->getPosition().y > 5000; },
        [](Map&, const std::unique_ptr<Player>& player){ player->takeDamage(100000); }
    );
}
std::function<bool(const Map&,const std::unique_ptr<Player>&)> Level1::getGameOverCondition() const{
    return [](const Map&,const std::unique_ptr<Player>& player){
        return player->isDead() || player->getPosition().y > 5000;
    };
}
std::function<bool(const Map&,const std::unique_ptr<Player>&)> Level1::getGameWinCondition() const{
    return [](const Map& map,const std::unique_ptr<Player>& player){
        return map.areAllEnemiesDead();
    };
}
void Level1::start() {
    mActionMusic->play();
}
void Level1::stop() {
    mActionMusic->stop();
}
Level2::Level2() {
}
void Level2::load(std::function<void(float)>&& progress) {
    if(mLoaded){
        progress(1);
        return;
    }
    mActionMusic = SoundContainer::getInstance().getMusic("action");
    progress(0.1);
    SingleStringSprite::imageContainer::getInstance().loadSingleImage("level2_background", "assets/images/level2_background.jpg");
    auto texture = SingleStringSprite::imageContainer::getInstance().getSingleTexture("level2_background");
    texture->setRepeated(true);
    mBackground.setTexture(*texture);
    mBackground.setTextureRect(sf::IntRect{ -200, -200, 30000, 5000});
    progress(1);
    mLoaded = true;
}
void Level2::operator()(Map& map) {
    map.createPlayer(sf::Vector2u{700, 400});
    map.printf(
            "#                                                                                                          #\n"
            "#     LLLLLL                         LLLLLL                             LLLLL                              #\n"
            "#                                                                                                          #\n"
            "#                    F                                F                                      F             #\n"
            "#             ##################                 ##################                    #################   #\n"
            "#                                                                                                          #\n"
            "#                                                                                                          #\n"
            "#           F              P               F            P              F                            PP     #\n"
            "############################################################################################################\n",
            sf::Vector2u{50, 50}, 
            PlatformFactory( SingleStringSprite::imageContainer::getInstance().getSingleTexture("platform")), 
            LiftFactory(200, 1, 'L',  SingleStringSprite::imageContainer::getInstance().getSingleTexture("platform")),
            LaserFlyFactory(), PlayerSpawns());
    map.addPlayerScript(
        [](const Map&, const std::unique_ptr<Player>& player){ return player->getPosition().y > 5000; },
        [](Map&, const std::unique_ptr<Player>& player){ player->takeDamage(100000); }
    );
    map.setBackground(mBackground);
    map.addPlayerScript(
        [](const Map&, const std::unique_ptr<Player>& player){ return player->getPosition().y > 5000; },
        [](Map&, const std::unique_ptr<Player>& player){ player->takeDamage(100000); }
    );
    map.load();
}
std::function<bool(const Map&,const std::unique_ptr<Player>&)> Level2::getGameOverCondition() const{
    return [](const Map&,const std::unique_ptr<Player>& player){
        return player->isDead();
    };
}
std::function<bool(const Map&,const std::unique_ptr<Player>&)> Level2::getGameWinCondition() const{
    return [](const Map& map,const std::unique_ptr<Player>& player){
        return map.areAllEnemiesDead();
    };
}
void Level2::start() {
    mActionMusic->play();
}
void Level2::stop() {
    mActionMusic->stop();
}