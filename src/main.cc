#include "init.hpp"

#include <SFML/Network.hpp>
#include "network.hpp"

int main()
{
    
    GameConfig config = {
        sf::Vector2u{ 800, 600 }, // window size
        60, // frame limit
        sf::FloatRect{ 0, 0, 800, 600}, // initial viewport
        40002, // port
        b2Vec2{ 0, 9.81} // gravity
    };  
    try{
        InitializeAll();
        RunGame(config);
    }catch(std::runtime_error e){
        log(e.what());
    }
    return 0;
}
