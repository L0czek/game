#include "sound.hpp"

SoundContainer& SoundContainer::getInstance() {
	static SoundContainer mSoundContainer;
	return mSoundContainer;
}
sf::SoundBuffer& SoundContainer::getSoundBuffer(const std::string& name) {
	auto it = mSoundBuffer.find(name);
	if(it == mSoundBuffer.end()){
		throw std::runtime_error("Error no such sound");
	}
	return it->second;
}
std::shared_ptr<sf::Music> SoundContainer::getMusic(const std::string& name) {
	auto it = mMusic.find(name);
	if(it == mMusic.end()){
		throw std::runtime_error("Error no such sound");
	}
	return it->second;
}
void SoundContainer::loadSoundBuffer(const std::string& id,const std::string& fileName) {
	sf::SoundBuffer buffer;
	if(!buffer.loadFromFile(fileName)){
		throw std::runtime_error("Error cannot load sound");
	}
	mSoundBuffer.insert(std::make_pair(id, std::move(buffer)));
}
void SoundContainer::loadMusic(const std::string& id,const std::string& fileName) {
	std::shared_ptr<sf::Music> music = std::make_shared<sf::Music>();
	if(!music->openFromFile(fileName)){
		throw std::runtime_error("Error cannot load music");
	}
	mMusic.insert(std::make_pair(id, music));
}