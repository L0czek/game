#include "screen.hpp"
#include "fontContainer.hpp"

InitialScreen::InitialScreen(Game& mGame,sf::RenderWindow& mWindow) :Game::Screen(mGame, mWindow){
	mBackground.setTexture(*SingleStringSprite::imageContainer::getInstance().getSingleTexture("init_back"));
	mBackground.setScale(0.6, 0.6);
	mIntroMusic = SoundContainer::getInstance().getMusic("intro");
	mIntroMusic->setLoop(true);
	mIntroMusic->setVolume(50);
	mIntroMusic->play();
}
void InitialScreen::create() {
	auto size = getWindow().getSize();
	mStartButton.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mStartButton.setText("Start Game");
	mStartButton.setBounds(convert<int>(sf::Vector2f{size.x / 2.f, size.y / 2.f}),sf::Vector2i{200, 50});
	mStartButton.setColor(sf::Color::Red);
	mStartButton.onClick(std::bind(&InitialScreen::startCallback, this));

	mExitButton.setBounds(convert<int>(sf::Vector2f{size.x / 2.f, size.y / 2.f + 100}),sf::Vector2i{70, 50});
	mExitButton.setColor(sf::Color::Red);
	mExitButton.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mExitButton.setText("Exit");
	mExitButton.onClick(std::bind(&InitialScreen::exitCallback, this));

	mMultiHost.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mMultiHost.setText("MultiPlayer Host");
	mMultiHost.setBounds(convert<int>(sf::Vector2f{size.x / 2.f, size.y * 0.8f}),sf::Vector2i{300, 50});
	mMultiHost.setColor(sf::Color::Red);
	mMultiHost.onClick(std::bind(&InitialScreen::multiHostCallback, this));
	
	mMultiClient.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mMultiClient.setText("MultiPlayer Client");
	mMultiClient.setBounds(convert<int>(sf::Vector2f{size.x / 2.f, size.y * 0.9f}),sf::Vector2i{300, 50});
	mMultiClient.setColor(sf::Color::Red);
	mMultiClient.onClick(std::bind(&InitialScreen::multiClientCallback, this));

	mNick.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mNick.setBounds(convert<int>(sf::Vector2f{size.x / 2.f, size.y * 0.1f}),sf::Vector2i{300, 50});
	mNick.setColor(sf::Color::Red);
	mNick.registerCallback();
}
void InitialScreen::update() {
	auto mousePosition = sf::Mouse::getPosition(getWindow());
	mStartButton.update(mousePosition);
	mExitButton.update(mousePosition);
	mMultiHost.update(mousePosition);
	mMultiClient.update(mousePosition);
}
void InitialScreen::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mBackground, states);
	target.draw(mNick, states);
	target.draw(mStartButton, states);
	target.draw(mExitButton, states);
	target.draw(mMultiHost, states);
	target.draw(mMultiClient, states);
}
void InitialScreen::exitCallback() {
	getWindow().close();
}
void InitialScreen::startCallback() {
	mIntroMusic->stop();
	auto nick = mNick.getString();
	if(nick != ""){
		getGame().setNick(nick);
		getGame().setupLevelSingle(0, 0);
	}
}
void InitialScreen::multiHostCallback() {
	mIntroMusic->stop();
	getGame().setNick(mNick.getString());
	getGame().startServer();
	getGame().getServer().acceptConnections();
	getGame().displayHostScreen();
}
void InitialScreen::multiClientCallback() {
	mIntroMusic->stop();
	getGame().setNick(mNick.getString());
	getGame().startClient();
	getGame().displayClientScreen();
}
LoadingScreen::LoadingScreen(Game& mGame,sf::RenderWindow& mWindow) :Game::LoadingScreen(mGame, mWindow){
	mBackground.setTexture(*SingleStringSprite::imageContainer::getInstance().getSingleTexture("load_back"));
	mBackground.setScale(0.6, 0.6);
	mLoadingMusic = SoundContainer::getInstance().getMusic("loading");
	mLoadingMusic->setVolume(50);
	mLoadingMusic->setLoop(true);
}
void LoadingScreen::create() {
	auto size = getWindow().getSize();
	auto size_x = size.x * 0.8f;
	mProgressBar.setSize(sf::Vector2f{size_x, 10.0f});
	auto pos_x = (size.x - size_x) / 2.0f;
	auto pos_y = size.y * 0.8f; 
	mProgressBar.setPosition(sf::Vector2f{pos_x, pos_y});
	mProgressBar.setColor(sf::Color::Red, sf::Color::Red);
	mLoadingText.setFont(FontContainer::getInstance().getDefaultFont());
	mLoadingText.setString(sf::String("Loading..."));
	mLoadingText.setFillColor(sf::Color::Yellow);
	auto textSize = mLoadingText.getGlobalBounds();
	mLoadingText.setOrigin(sf::Vector2f{textSize.width / 2.0f, textSize.height / 2.0f});
	mLoadingText.setPosition(size.x / 2.0f, size.y * 0.5f);
	updateProgress(0);
	mLoadingMusic->play();
}
void LoadingScreen::update() {

}
void LoadingScreen::updateProgress(float progress) {
	mProgressBar.update(progress);
	getWindow().clear();
	getWindow().draw(*this);
	getWindow().display();
	if(progress >= 1){
		mLoadingMusic->stop();
	}
}
void LoadingScreen::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mBackground, states);
	target.draw(mLoadingText, states);
	target.draw(mProgressBar, states);
}

GameOverScreen::GameOverScreen(Game& mGame,sf::RenderWindow& mWindow) :Game::Screen(mGame, mWindow) {
	mFailMusic = SoundContainer::getInstance().getMusic("fail");
	mFailMusic->setVolume(50);
}
void GameOverScreen::create() {
	auto size = getWindow().getSize();
	auto leftTopCorner = getGame().getMap().getView().getCenter() - convert<float>(size)/2;
	auto middle = getGame().getMap().getView().getCenter() ;
	mRestart.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mRestart.setText("Restart");
	mRestart.setBounds(convert<int>(sf::Vector2f{middle.x, leftTopCorner.y  + size.y/ 2}),sf::Vector2i{200, 50});
	mRestart.setColor(sf::Color::Red);
	mRestart.onClick(std::bind(&GameOverScreen::restartCallback, this));

	mExit.setBounds(convert<int>(sf::Vector2f{middle.x, leftTopCorner.y + size.y/ 2 + 100}),sf::Vector2i{70, 50});
	mExit.setColor(sf::Color::Red);
	mExit.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mExit.setText("Exit");
	mExit.onClick(std::bind(&GameOverScreen::exitCallback, this));

	mGameOverText.setFont(FontContainer::getInstance().getDefaultFont());
	mGameOverText.setString(sf::String("Game Over"));
	mGameOverText.setFillColor(sf::Color::Yellow);
	auto textSize = mGameOverText.getGlobalBounds();
	mGameOverText.setOrigin(sf::Vector2f{textSize.width / 2, textSize.height / 2});
	mGameOverText.setPosition(middle.x, leftTopCorner.y  + size.y* 0.3);
	mFailMusic->stop();
	mFailMusic->play();
	if(getGame().isClient()){
		getGame().getClient().onReset([this](){
			this->mRestartReq = true;
		});
	}
}
void GameOverScreen::update() {
	auto mousePosition = sf::Mouse::getPosition(getWindow());
	auto view = getWindow().getView();
	mousePosition += convert<int>(view.getCenter() - view.getSize() /2);
	auto &game = getGame();
	if((game.isHost() && game.getMap().areAllPlayersDead()) || !getGame().isInNetworkMode()){
		mRestart.update(mousePosition);
		mDrawRestart = true;
	}else{
		mDrawRestart = false;
	}
	mExit.update(mousePosition);
	if(mRestartReq){
		mRestartReq = false;
		restartCallback();
	}
}
void GameOverScreen::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mGameOverText, states);
	if(mDrawRestart){
		target.draw(mRestart, states);
	}
	target.draw(mExit, states);
}
void GameOverScreen::exitCallback() {
	getWindow().close();
}
void GameOverScreen::restartCallback() {
	Game& game = getGame();
	Map& map = game.getMap();
	if(!game.isInNetworkMode()){
		game.setupLevelSingle(game.getLevelIndex(), game.getLoadingScreenIndex());
	}else{
		if(game.isHost()){
			LevelReset packet;
			game.getServer().sendToAll(packet);
			game.setupLevelHost(game.getLevelIndex(), game.getLoadingScreenIndex());
		}else{
			game.setupLevelClient(game.getLevelIndex(), game.getLoadingScreenIndex());
		}
	}
}
GameWinScreen::GameWinScreen(Game& mGame,sf::RenderWindow& mWindow) :Game::Screen(mGame, mWindow) {
	mSuccessMusic = SoundContainer::getInstance().getMusic("success");
	mSuccessMusic->setVolume(50);
}
void GameWinScreen::create() {
	auto size = getWindow().getSize();
	auto leftTopCorner = getGame().getMap().getView().getCenter() - convert<float>(size)/2;
	auto middle = leftTopCorner + convert<float>(size) /2;
	bool mIsNextLevel;
	if(getGame().isNextLevel()){
		mNextLevel.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
		mNextLevel.setText("Next Level");
		mNextLevel.setBounds(convert<int>(sf::Vector2f{middle.x, leftTopCorner.y + size.y / 2}),sf::Vector2i{200, 50});
		mNextLevel.setColor(sf::Color::Red);
		mNextLevel.onClick(std::bind(&GameWinScreen::nextLevelCallback, this));
		mIsNextLevel = true;
	}else{
		mIsNextLevel = false;
	}

	mExit.setBounds(convert<int>(sf::Vector2f{middle.x, leftTopCorner.y+ size.y / 2 + 100}),sf::Vector2i{70, 50});
	mExit.setColor(sf::Color::Red);
	mExit.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mExit.setText("Exit");
	mExit.onClick(std::bind(&GameWinScreen::exitCallback, this));

	mGameWinText.setFont(FontContainer::getInstance().getDefaultFont());
	mGameWinText.setString(sf::String("Level Completed"));
	mGameWinText.setFillColor(sf::Color::Yellow);
	auto textSize = mGameWinText.getGlobalBounds();
	mGameWinText.setOrigin(sf::Vector2f{textSize.width / 2, textSize.height / 2});
	mGameWinText.setPosition(middle.x, leftTopCorner.y+ size.y * 0.2);
	mSuccessMusic->stop();
	mSuccessMusic->play();
	if(getGame().isClient()){
		getGame().getClient().onNextLevel([this](){
			this->mNextLevelRequest = true;
		});
	}
	mDrawNextLevel = mIsNextLevel & (getGame().isHost() || !getGame().isInNetworkMode());
}
void GameWinScreen::update() {
	auto mousePosition = sf::Mouse::getPosition(getWindow());
	auto view = getWindow().getView();
	mousePosition += convert<int>(view.getCenter() - view.getSize() /2);
	if(mDrawNextLevel){
		mNextLevel.update(mousePosition);
	}
	mExit.update(mousePosition);
	if(mNextLevelRequest){
		mNextLevelRequest = false;
		nextLevelCallback();
	}
}
void GameWinScreen::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mGameWinText, states);
	if(mDrawNextLevel){
		target.draw(mNextLevel, states);
	}
	target.draw(mExit, states);
}
void GameWinScreen::exitCallback() {
	getWindow().close();
}
void GameWinScreen::nextLevelCallback() {
	getGame().nextLevel();
}
GameMultiHostScreen::GameMultiHostScreen(Game& mGame,sf::RenderWindow& mWindow) :Game::Screen(mGame, mWindow) {
	mBackground.setTexture(*SingleStringSprite::imageContainer::getInstance().getSingleTexture("net"));
}
void GameMultiHostScreen::create() {
	auto size = getWindow().getSize();
	mStartGame.setBounds(convert<int>(sf::Vector2f{size.x * 0.2f, size.y * 0.2f}), sf::Vector2i{300, 50});
	mStartGame.setColor(sf::Color::Red);
	mStartGame.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mStartGame.setText("Start Game");
	mStartGame.onClick(std::bind(&GameMultiHostScreen::startCallback, this));

	mBack.setBounds(convert<int>(sf::Vector2f{size.x * 0.8f, size.y * 0.2f}), sf::Vector2i{300, 50});
	mBack.setColor(sf::Color::Red);
	mBack.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mBack.setText("Back");
	mBack.onClick(std::bind(&GameMultiHostScreen::backCallback, this));
}
void GameMultiHostScreen::update() {
	auto mousePosition = sf::Mouse::getPosition(getWindow());
	mStartGame.update(mousePosition);
	mBack.update(mousePosition);
	const auto &clients = getGame().getServer().getClients();
	if(mNicks.size() != clients.size()){
		auto size = getWindow().getSize();
		std::size_t y = size.y * 0.4;
		mNicks.resize(clients.size());
		for(auto &i : mNicks){
			i.setFillColor(sf::Color::Yellow);
			i.setPosition(size.x / 2, y);
			i.setFont(FontContainer::getInstance().getDefaultFont());
			y += 100;
		}
	}
	auto it = mNicks.begin();
	for(const auto &i : clients){
		it->setString(i.first);
		it++;
	}
	getGame().getServer().update();
}
void GameMultiHostScreen::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mBackground, states);
	target.draw(mStartGame, states);
	target.draw(mBack, states);
	for(const auto &i : mNicks){
		target.draw(i, states);
	}
}
void GameMultiHostScreen::startCallback() {
	getGame().setupLevelHost(0, 0);	
}
void GameMultiHostScreen::backCallback() {
	getGame().getServer().stopAccepting();
	getGame().displayInitialScreen();
}
GameMultiClientScreen::GameMultiClientScreen(Game& mGame,sf::RenderWindow& mWindow) :Game::Screen(mGame, mWindow){
	mBackground.setTexture(*SingleStringSprite::imageContainer::getInstance().getSingleTexture("net"));	
}
void GameMultiClientScreen::create() {
	auto size = getWindow().getSize();
	mIp.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mIp.setBounds(convert<int>(sf::Vector2f{size.x / 2.f, size.y / 2.f}),sf::Vector2i{300, 50});
	mIp.setColor(sf::Color::Red);

	mConnect.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mConnect.setBounds(convert<int>(sf::Vector2f{size.x / 2.f, size.y * 0.3f}),sf::Vector2i{300, 50});
	mConnect.setText("Connect");
	mConnect.setColor(sf::Color::Red);
	mConnect.onClick(std::bind(&GameMultiClientScreen::connectCallback, this));

	mBack.setFont(FontContainer::getInstance().getDefaultFont(), 30, sf::Color::Yellow);
	mBack.setBounds(convert<int>(sf::Vector2f{size.x / 2.f, size.y * 0.7f}),sf::Vector2i{300, 50});
	mBack.setText("Back");
	mBack.setColor(sf::Color::Red);
	mBack.onClick(std::bind(&GameMultiClientScreen::backCallback, this));

	getGame().getClient().onLoad(std::bind(&GameMultiClientScreen::loadCallback, this));
	mIp.registerCallback();
}
void GameMultiClientScreen::update() {
	auto mousePosition = sf::Mouse::getPosition(getWindow());
	mConnect.update(mousePosition);
	mBack.update(mousePosition);
	getGame().getClient().update();
}
void GameMultiClientScreen::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mBackground, states);
	target.draw(mIp, states);
	target.draw(mConnect, states);
	target.draw(mBack, states);
}
void GameMultiClientScreen::connectCallback() {
	getGame().getClient().connectTo(mIp.getString(), getGame().getConfig().mPort);
}
void GameMultiClientScreen::backCallback() {
	ExitPacket packet;
	packet.mName = getGame().getNick();
	getGame().getClient().send(packet);
	getGame().displayInitialScreen();
}
void GameMultiClientScreen::loadCallback() {
	getGame().setupLevelClient(0, 0);
}