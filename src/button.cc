#include "button.hpp"
#include "common.hpp"

Button::Button() {

}
void Button::setBounds(const sf::Vector2i& position,const sf::Vector2i& size) {
	mShape.setSize(convert<float>(size));
	mShape.setOrigin(convert<float>(size) / 2);
	mShape.setPosition(convert<float>(position));
	mText.setPosition(convert<float>(position) - sf::Vector2f{0, 9});
}
void Button::update(const sf::Vector2i& mousePos) {
	if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mShape.getGlobalBounds().contains(convert<float>(mousePos)) && mOnClick){
		if(!mClicked){
			static sf::Clock clock;
			if(clock.getElapsedTime().asSeconds() > 0.2){
				mOnClick();
				mClicked = true;
				clock.restart();
			}
		}
	}else{
		mClicked = false;
	}
}
void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mShape, states);
	target.draw(mText, states);
}
void Button::setColor(const sf::Color& color) {
	mShape.setFillColor(color);
}
void Button::setText(const std::string& text) {
	mText.setString(sf::String(text));
	auto bounds = mText.getGlobalBounds();
	mText.setOrigin(sf::Vector2f{bounds.width, bounds.height} / 2);
}
void Button::setFont(const sf::Font& font,unsigned int fontSize,const sf::Color& color) {
	mText.setFont(font);
	mText.setFillColor(color);
	mText.setCharacterSize(fontSize);
}