#include "network.hpp"
#include <thread>
#include <functional>
#include "player.hpp"

using namespace std::chrono_literals;

NetworkServer::NetworkServer(short unsigned int port,const std::string& name) :mName(name) {
	mTcpListener.listen(port);
	mTcpListener.setBlocking(false);
}
void NetworkServer::acceptConnections() {
	mAcceptConnections = true;
	mNetworkThread = std::make_unique<std::thread>(std::bind(&NetworkServer::acceptConnectionsLoop, this));
}
void NetworkServer::stopAccepting() {
	mAcceptConnections = false;
	if(mNetworkThread){
		mNetworkThread->join();
		mNetworkThread.reset();
	}
}
void NetworkServer::acceptConnectionsLoop() {
	std::unique_ptr<sf::TcpSocket> socket = std::make_unique<sf::TcpSocket>();
	socket->setBlocking(false);
	while(mAcceptConnections){
		if(!socket){
			socket = std::make_unique<sf::TcpSocket>();
			socket->setBlocking(false);
		}
		if(mTcpListener.accept(*socket) == sf::Socket::Done){
			sf::Packet rawPacket;
			socket->receive(rawPacket);
			std::string packetId;
			rawPacket >> packetId;
			if(packetId == HelloPacket::getId()){
				HelloPacket packet;
				packet.deserialize(rawPacket);
				mClients.insert(std::make_pair(std::move(packet.mName), std::move(socket)));
			}
		}
		std::this_thread::sleep_for(5ms);
	}
}
void NetworkServer::clearQueue() {
	mClients.clear();
}
void NetworkServer::update() {
	for(auto &i : mClients){
		sf::Packet packet;
		auto state = i.second->receive(packet);
		if(state == sf::Socket::Disconnected){
			mClients.erase(i.first);
		}else if(state == sf::Socket::Done){
			std::string id;
			packet >> id;
			if(id == ExitPacket::getId()){
				mClients.erase(i.first);
			}
		}
	}
}
void NetworkServer::send(const std::string& target,Packet& packet) {
	auto it = mClients.find(target);
	if(it != mClients.end()){
		sf::Packet rawPacket = packet.serialize();
		sf::Socket::Status status;
		do{
			status = it->second->send(rawPacket);
			if(status == sf::Socket::Error){
				mClients.erase(target);
				return;
			}
		}while(status == sf::Socket::Partial);
	}
}
void NetworkServer::sendToAll(Packet& packet) {
	for(auto &i : mClients){
		send(i.first, packet);
	}
}
NetworkClient::NetworkClient(const std::string& name) :mName(name) {
	mSocket.setBlocking(false);
}
void NetworkClient::connectTo(const std::string& ip,short unsigned int port) {
	mSocket.setBlocking(true);
	sf::Socket::Status status;
	if(mSocket.connect(ip, port) != sf::Socket::Done){
		return;
	}
	mSocket.setBlocking(false);
	HelloPacket packet;
	packet.mName = mName;
	send(packet);
	mConnected = true;
}
void NetworkClient::send(Packet& packet) {
	if(!mConnected){
		return;
	}
	auto rawPacket = packet.serialize();
	sf::Socket::Status status;
	do{
		status = mSocket.send(rawPacket);
		if(status == sf::Socket::Error){
			throw std::runtime_error("Network error");
		}
	}while(status == sf::Socket::Partial);
}
void NetworkClient::update() {
	sf::Packet rawPacket;
	auto status = mSocket.receive(rawPacket);
	if(status == sf::Socket::Disconnected){
		throw std::runtime_error("disconnected");
	}else if(status == sf::Socket::Done){
		std::string id;
		rawPacket >> id;
		if(id == LoadLevel::getId()){
			if(mOnLoad){
				mOnLoad();
			}
		}
	}
}
void NetworkServer::waitForClientsToLoad() {
	sf::Packet packet;
	bool clientConnected = false;
	for(auto &i : mClients){
		clientConnected = false;
		do{
			auto status = i.second->receive(packet);
			if(status == sf::Socket::Disconnected){
				mClients.erase(i.first);
				break;
			}else if(status == sf::Socket::Done){
				std::string id;
				packet >> id;
				if(id == LevelLoaded::getId()){
					clientConnected = true;
				}
			}
			std::this_thread::sleep_for(5ms);
		}while(!clientConnected);
	}
}
StartGame NetworkClient::waitForServerToLoad() {
	sf::Packet packet;
	do{
		auto status = mSocket.receive(packet);
		if(status == sf::Socket::Disconnected){
			throw std::runtime_error("Disconnected");
		}else if(status == sf::Socket::Done){
			std::string id;
			packet >> id;
			if(id == StartGame::getId()){
				StartGame p;
				p.deserialize(packet);
				return p;
			}
		}
	}while(true);
}
void NetworkServer::setDynamicEntitiesState(const std::set<std::unique_ptr<DynamicEntity>>& set) {	
	for(const auto &i : set){
		auto packet = i->getState();
		mDynamicEntities[i->getNetworkId()] = std::move(packet);
	}
}
void NetworkServer::setEnemiesState(const std::set<std::unique_ptr<Enemy>>& set) {
	for(const auto &i : set){
		auto packet = i->getState();
		mEnemies[i->getNetworkId()] = std::move(packet);
	}
}
void NetworkClient::getDynamicEntitiesState(std::set<std::unique_ptr<DynamicEntity>>& set) {
	for(auto &i : set){
		auto it = mDynamicEntities.find(i->getNetworkId());
		if(it != mDynamicEntities.end()){
			//std::lock_guard lock(mDataLock);
			i->setState(it->second);			
		}
	}
}
void NetworkClient::getEnemiesState(const std::set<std::unique_ptr<Enemy>>& set) {
	for(auto &i : set){
		auto it = mEnemies.find(i->getNetworkId());
		if(it != mEnemies.end()){
			//std::lock_guard lock(mDataLock);
			i->setState(it->second);
		}
	}
}
void NetworkServer::syncLoop() {
	while(mRunSyncLoop){
		sf::Packet rawPacket;
		for(auto &i : mClients){
			auto status = i.second->receive(rawPacket);
			if(status == sf::Socket::Disconnected){
				
			}else if(status == sf::Socket::Done){
				std::string id;
				rawPacket >> id;
				if(id == PlayerSyncPacket::getId()){
					PlayerSyncPacket packet;
					packet.deserialize(rawPacket);
					mPlayers[i.first] = packet;
				}
			}
		}
		if(mNewDataToSync){
			for(auto &i : mDynamicEntities){
				std::lock_guard lock(mDataLock);
				sendToAll(i.second);
			}
			for(auto &i : mEnemies){
				std::lock_guard lock(mDataLock);
				sendToAll(i.second);
			}
			for(auto &i : mPlayers){
				i.second.mChangeHealth = true;
				std::lock_guard lock(mDataLock);
				sendToAll(i.second);
			}
			mNewDataToSync = false;
		}
		std::this_thread::sleep_for(5ms);
	}
}
void NetworkClient::syncLoop() {
	while(mRunSyncLoop){
		sf::Packet rawPacket;
		auto status = mSocket.receive(rawPacket);
		if(status == sf::Socket::Disconnected){
			return;
		}else if(status == sf::Socket::Done){
			std::string id;
			rawPacket >> id;
			if(id == DynamicEntitySyncPacket::getId()){
				DynamicEntitySyncPacket packet;
				packet.deserialize(rawPacket);
				std::lock_guard lock(mDataLock);
				mDynamicEntities[packet.mId] = packet;
			}else if(id == EnemySyncPacket::getId()){
				EnemySyncPacket packet;
				packet.deserialize(rawPacket);
				std::lock_guard lock(mDataLock);
				mEnemies[packet.mId] = packet;
			}else if(id == PlayerSyncPacket::getId()){
				PlayerSyncPacket packet;
				packet.deserialize(rawPacket);
				std::lock_guard lock(mDataLock);
				mPlayers[packet.mId] = packet;
			}else if(id == LevelReset::getId()){
				if(mOnReset){
					mOnReset();
				}
			}else if(id == NextLevel::getId()){
				if(mOnNextLevel){
					mOnNextLevel();
				}
			}
		}
	}
	mSocket.setBlocking(false);
}
void NetworkServer::startSync() {
	mRunSyncLoop = true;
	mPlayers.clear();
	mSyncThread = std::make_unique<std::thread>(std::bind(&NetworkServer::syncLoop, this));
}
void NetworkServer::stopSync() {
	if(!mSyncThread){
		return;
	}
	mRunSyncLoop = false;
	mSyncThread->join();
	mSyncThread.reset();
}
void NetworkClient::startSync() {
	mRunSyncLoop = true;
	mPlayers.clear();
	mSyncThread = std::make_unique<std::thread>(std::bind(&NetworkClient::syncLoop, this));
}
void NetworkClient::stopSync() {
	if(!mSyncThread){
		return;
	}
	mRunSyncLoop = false;
	mSyncThread->join();
	mSyncThread.reset();
}
void NetworkServer::sync() {
	mNewDataToSync = true;
}
sf::Vector2u NetworkClient::getPlayerPosition() {
	sf::Packet rawPacket;
	do{
		auto status = mSocket.receive(rawPacket);
		if(status == sf::Socket::Disconnected){
			throw std::runtime_error("disconnected");
		}else if(status == sf::Socket::Done){
			std::string id;
			rawPacket >> id;
			if(id == PlayerSet::getId()){
				PlayerSet packet;
				packet.deserialize(rawPacket);
				return std::move(packet.mPosition);
			}
		}
		std::this_thread::sleep_for(1ms);
	}while(true);
}
PlayerList NetworkClient::getPlayerList() {
	sf::Packet rawPacket;
	do{
		auto status = mSocket.receive(rawPacket);
		if(status == sf::Socket::Disconnected){
			throw std::runtime_error("disconnected");
		}else if(status == sf::Socket::Done){
			std::string id;
			rawPacket >> id;
			if(id == PlayerList::getId()){
				PlayerList packet;
				packet.deserialize(rawPacket);
				return std::move(packet);
			}
		}
		std::this_thread::sleep_for(1ms);
	}while(true);
}
void NetworkServer::setPlayerState(const std::unique_ptr<Player>& player) {
	mPlayers[mName] = player->getState();
}
void NetworkServer::getNetworkPlayersState(std::vector<std::unique_ptr<Player>>& players) {
	for(auto &i : players){
		auto it = mPlayers.find(i->getNick());
		if(it != mPlayers.end()){
			it->second.mChangeHealth = false;
			it->second.mHealth = i->getHealth();
			if(it->second.mAttackRequests.size() > 0){
				while(!it->second.mAttackRequests.empty()){
					auto [ position, direction] = it->second.mAttackRequests.front(); it->second.mAttackRequests.pop_front();
					i->attack(position, direction); 
				}
			}
			i->setState(it->second);
		}
	}
}
void NetworkClient::setPlayerState(std::unique_ptr<Player>& player) {
	auto state = player->getState();
	state.mChangeHealth = false;
	send(state);
	auto it = mPlayers.find(mName);
	if(it != mPlayers.end()){
		player->setHealth(it->second.mHealth);
		player->quickUpdate();
	}
}
void NetworkClient::getNetworkPlayersState(std::vector<std::unique_ptr<Player>>& players) {
	for(auto &i : players){
		auto it = mPlayers.find(i->getNick());
		if(it != mPlayers.end()){
			it->second.mChangeHealth = true;
			i->setState(it->second);
		}
	}
}
NetworkServer::~NetworkServer() {
	stopAccepting();
	stopSync();
}
NetworkClient::~NetworkClient() {
	stopSync();
}