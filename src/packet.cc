#include "packet.hpp"

sf::Packet HelloPacket::serialize() const{
	sf::Packet packet;
	packet << getId() << mName;
	return std::move(packet);
}
bool HelloPacket::deserialize(sf::Packet& packet) {
	return packet >> mName;
}
sf::Packet ExitPacket::serialize() const{
	sf::Packet packet;
	packet << getId() << mName;
	return packet;
}
bool ExitPacket::deserialize(sf::Packet& packet) {
	return packet >> mName;
}

sf::Packet LoadLevel::serialize() const{
	sf::Packet packet;
	packet << getId();
	return packet;
}
bool LoadLevel::deserialize(sf::Packet& packet) {
	return true;
}

sf::Packet LevelLoaded::serialize() const{
	sf::Packet packet;
	packet << getId();
	return packet;
} 
bool LevelLoaded::deserialize(sf::Packet& packet) {
	return true;
}

sf::Packet StartGame::serialize() const{
	sf::Packet packet;
	packet << getId() << mPlayerPosition.x << mPlayerPosition.x;
	return packet;
} 
bool StartGame::deserialize(sf::Packet& packet) {
	return packet >> mPlayerPosition.x >> mPlayerPosition.x;
}

sf::Packet DynamicEntitySyncPacket::serialize() const{
	sf::Packet packet;
	packet << getId() << mId << mPosition.x << mPosition.y << mVelocity.x << mVelocity.y;
	return packet;
}
bool DynamicEntitySyncPacket::deserialize(sf::Packet& packet) {
	return packet >> mId >> mPosition.x >> mPosition.y >> mVelocity.x >> mVelocity.y;
}

sf::Packet EnemySyncPacket::serialize() const{
	sf::Packet rawPacket;
	rawPacket << getId() << mId << mPosition.x << mPosition.y <<  mVelocity.x << mVelocity.y <<  mImage <<  mSpriteCordinates.x << mSpriteCordinates.y <<  mWeaponSpriteConrdinates.x << mWeaponSpriteConrdinates.y <<  mScale.x << mScale.y << mState << mHealth;
	return rawPacket;
} 	
bool EnemySyncPacket::deserialize(sf::Packet& packet) {
	return packet >> mId >> mPosition.x >> mPosition.y >>  mVelocity.x >> mVelocity.y >>  mImage >>  mSpriteCordinates.x >> mSpriteCordinates.y >>  mWeaponSpriteConrdinates.x >> mWeaponSpriteConrdinates.y >>  mScale.x >> mScale.y >> mState >> mHealth;
}
sf::Packet PlayerSyncPacket::serialize() const{
	sf::Packet packet;
	packet << getId() << mId << mPosition.x << mPosition.y << mVelocity.x << mVelocity.y << mSpriteCordinates.x << mSpriteCordinates.y << mScale.x << mScale.y << mImage << mHealth << mChangeHealth << mEquippedWeapon << static_cast<sf::Uint64>(mAttackRequests.size());
	for(auto &i : mAttackRequests){
		packet << i.first.x << i.first.y << i.second.x << i.second.y;
	}
	return packet;
}
bool PlayerSyncPacket::deserialize(sf::Packet& packet) {
	bool status = packet >> mId >> mPosition.x >> mPosition.y >> mVelocity.x >> mVelocity.y >> mSpriteCordinates.x >> mSpriteCordinates.y >> mScale.x >> mScale.y >> mImage >> mHealth >> mChangeHealth >> mEquippedWeapon;
	if(!status){
		return false;
	}
	sf::Uint64 n;
	if(!(packet >> n)){
		return false;
	}
	for(std::size_t i=0; i < n; ++i){
		sf::Vector2f position;
		sf::Vector2f direction;
		status = packet >> position.x >> position.y >> direction.x >> direction.y;
		if(!status){
			return false;
		}
		mAttackRequests.emplace_back(std::move(position), std::move(direction));
	}
	return true;
}
sf::Packet PlayerSet::serialize() const{
	sf::Packet packet;
	packet << getId() << static_cast<sf::Uint32>(mPosition.x) << static_cast<sf::Uint32>(mPosition.y);
	return packet;
}
bool PlayerSet::deserialize(sf::Packet& packet) {
	return packet >> mPosition.x >> mPosition.y;
}
sf::Packet PlayerList::serialize() const{
	sf::Packet packet;
	packet << getId() << static_cast<sf::Uint64>(mPlayers.size());
	for(const auto &i : mPlayers){
		packet << i.first << i.second.x << i.second.y;
	}
	return packet;
}
bool PlayerList::deserialize(sf::Packet& packet) {
	sf::Uint64 n;
	packet >> n;
	for(std::size_t i=0; i < n; ++i){
		std::string name;
		sf::Vector2u position;
		packet >> name >> position.x >> position.y;
		mPlayers.emplace_back(std::make_pair(name, position));
	}
	return true;
}
sf::Packet LevelReset::serialize() const{
	sf::Packet packet;
	packet << getId();
	return packet;
}
bool LevelReset::deserialize(sf::Packet& packet) {
	return true;
}
sf::Packet NextLevel::serialize() const{
	sf::Packet packet;
	packet << getId();
	return packet;
}
bool NextLevel::deserialize(sf::Packet& packet) {
	return true;
}