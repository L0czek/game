#include "init.hpp"
#include "fontContainer.hpp"
#include "screen.hpp"
#include "levels.hpp"
#include "sound.hpp"

void InitializeAll() {
	FontContainer::getInstance().loadFromFile("bboron", "assets/font/bboron.ttf");
    FontContainer::getInstance().setDefaultFont();
    SingleStringSprite::imageContainer::getInstance().loadSingleImage("init_back", "assets/images/intro_background.jpg");
    SingleStringSprite::imageContainer::getInstance().loadSingleImage("load_back", "assets/images/loading_background.jpg");
    SingleStringSprite::imageContainer::getInstance().loadSingleImage("bsod", "assets/images/bsod.png");
    SingleStringSprite::imageContainer::getInstance().loadSingleImage("net", "assets/images/net.jpg");
   	SoundContainer::getInstance().loadMusic("intro", "assets/sounds/intro.wav");
   	SoundContainer::getInstance().loadMusic("loading", "assets/sounds/loading.wav");
   	SoundContainer::getInstance().loadMusic("fail", "assets/sounds/fail.wav");
   	SoundContainer::getInstance().loadMusic("success", "assets/sounds/success.wav");
}
void RunGame(const GameConfig& config) {
	static Game game(config);
	try{
    	game.createInitialScreen<InitialScreen>();
    	game.addLoadingScreen<LoadingScreen>();
    	game.addLevel<Level1>();
    	game.addLevel<Level2>();
    	game.displayInitialScreen();
    	game.allocateMap();
    	game.createGameOverScreen<GameOverScreen>();
    	game.createGameWinScreen<GameWinScreen>();
    	game.createGameMultiHostScreen<GameMultiHostScreen>();
      game.createGameMultiClientScreen<GameMultiClientScreen>();
    	game.loop();
	}catch(std::runtime_error e){
		log(e.what());
		game.bsod();
	}
}