#include "playerWeapon.hpp"
#include "common.hpp"
#include "rayCast.hpp"
#include "scale.hpp"
#include "player.hpp"
#include "laserFly.hpp"
#include "contactListener.hpp"
#include "random.hpp"

struct ShootVisitor :public EntityVisitor{
	float mDamage;
	b2Vec2 mImpuse;
	sf::Vector2f mShooterPosition;
	bool mGotToWall = false;
	ShootVisitor(float mDamage,b2Vec2 mImpuse,sf::Vector2f mShooterPosition) :mDamage(mDamage), mImpuse(mImpuse), mShooterPosition(mShooterPosition) {}
	void visit(Player& entity) override{
#ifdef __FIRENDLY_FIRE__
		entity.takeDamage(mDamage);
		entity.applyImpulse(mImpuse);	
#endif		
	}
	void visit(Platform& entity) override{
		mGotToWall = true;
	}
	void visit(Lift&) override{
		mGotToWall = true;
	}
	void visit(LaserFly& entity) override{
		if(!mGotToWall){
			entity.takeDamage(mDamage);
			entity.applyImpulse(mImpuse);
			entity.hitFrom(mShooterPosition);
		}
	}
	void apply(ContactInterface& interface) {
		interface.accept(*this);
	}
};

struct isTargetAlivePlayer :public EntityVisitor{
	bool isAlive = false;
	void visit(Player& entity) override{
		isAlive = !entity.isDead();		
	}
	void visit(Platform& entity) override{}
	void visit(Lift&) override{}
	void visit(LaserFly& entity) override{}
};

void AK47::shoot(const sf::Vector2f& position,const sf::Vector2f& direction) {
	if(mSound.getStatus() != sf::Sound::Status::Playing){
		mSound.play();
	}
	auto end = position + direction * 1000.0f;
	auto start = direction * 10 + position;
	ShootVisitor visitor{RandomGenerator::getInstance().getNumber(5.f, 10.f), Scale::SFML2Box(direction * 1000.0f), position };
	RayCast::forAllInRangeInOrder(mWorld, start, end, std::bind(&ShootVisitor::apply, &visitor, std::placeholders::_1));
	/*
	auto info = RayCast::findNearst(mWorld, start, end);
	if(info){
		auto hitInfo = std::move(*info);
		auto ptr = hitInfo.mTarget->GetBody()->GetUserData();
		if(!ptr){
			log("Waring empty UserData in BoxBody");
			return;
		}
		ShootVisitor visitor{ RandomGenerator::getInstance().getNumber(5.f, 10.f), Scale::SFML2Box(direction * 1000.0f), position };
		reinterpret_cast<ContactInterface*>(ptr)->accept(visitor);
	}*/
	
}
void AK47::reload() {

}

void BaseBallStick::attack(const sf::Vector2f& position,const sf::Vector2f& direction) {
	auto end = position + direction * 100.0f;
	auto info = RayCast::findNearst(mWorld, position, end);
	if(info){
		mSound.play();
		auto hitInfo = std::move(*info);
		auto ptr = hitInfo.mTarget->GetBody()->GetUserData();
		if(!ptr){
			log("Waring empty UserData in BoxBody");
			return;
		}
		ShootVisitor visitor{ RandomGenerator::getInstance().getNumber(500.f, 1000.f), Scale::SFML2Box(direction * 10000.0f), position };
		reinterpret_cast<ContactInterface*>(ptr)->accept(visitor);
	}
}