#include "lift.hpp"

#include "Box2D/Box2D.h"
#include "collisions.hpp"
#include "random.hpp"

Lift::Lift(b2World& world,const sf::Vector2u& position,const sf::Vector2u& size,float mHeight,float speed,const std::shared_ptr<sf::Texture>& texture,float friction) :mStartPoint(position), mHeight(mHeight), mSpeed(speed){
	b2BodyDef body;
	body.type = b2_kinematicBody;
	body.position.Set(Scale::SFML2Box(position.x), Scale::SFML2Box(position.y));
	mSprite = std::make_unique<SingleStringSprite>(world,body,texture);
    mSprite->setFixtureData(friction, 0.01, 0.2);
    mSprite->resize(size.x, size.y);
    mSprite->setUserData(this);
    mSprite->setOriginToMiddle();
    setCollisionFilter(mSprite->getFixture(), ObjectType::Platform, collideWithAll());
    mSprite->getBody()->SetLinearVelocity(b2Vec2{0, mSpeed});
    mSprite->update();

}
void Lift::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(*mSprite, states);
}
void Lift::update() {
	b2Body& body = *mSprite->getBody();
	auto elevation = mSprite->getPosition().y - mStartPoint.y;
	if(elevation <= 0){
		auto velocity = body.GetLinearVelocity();
		velocity.y = RandomGenerator::getInstance().getNumber(mSpeed/2, mSpeed);
		body.SetLinearVelocity(velocity);
	}else if(elevation >= mHeight){
		auto velocity = body.GetLinearVelocity();
		velocity.y = -RandomGenerator::getInstance().getNumber(mSpeed/2, mSpeed);
		body.SetLinearVelocity(velocity);
	}
	mSprite->update();
}
DynamicEntitySyncPacket Lift::getState() const{
	DynamicEntitySyncPacket packet;
	packet.mId = getNetworkId();
	packet.mPosition = mSprite->getBody()->GetPosition();
	packet.mVelocity = mSprite->getBody()->GetLinearVelocity();
	return packet;
}
void Lift::setState(const DynamicEntitySyncPacket& packet) {
	mSprite->getBody()->SetLinearVelocity(packet.mVelocity);
#ifdef __SYNC_POSITION__
		mSprite->getBody()->SetTransform(packet.mPosition, mSprite->getBody()->GetAngle());
#endif
	mSprite->update();
}