#include "fontContainer.hpp"

FontContainer& FontContainer::getInstance() {
	static FontContainer mInstance;
	return mInstance;
}
void FontContainer::loadFromFile(const std::string& name, const std::string& fileName) {
	sf::Font font;
	if(!font.loadFromFile(fileName)){
		throw std::runtime_error("Can not open font");
	}
	mFonts.insert(std::make_pair(name, std::move(font)));
}
void FontContainer::setDefaultFont(const std::string& name) {
	auto it = mFonts.find(name);
	if(it != mFonts.end()){
		throw std::runtime_error("No such font");
	}
	mDefaultFont = it;
}
void FontContainer::setDefaultFont() {
	mDefaultFont = mFonts.begin();
}
const sf::Font& FontContainer::getFont(const std::string& name) {
	auto it = mFonts.find(name);
	if(it != mFonts.end()){
		throw std::runtime_error("No such font");
	}
	return it->second;
}
const sf::Font& FontContainer::getDefaultFont() {
	if(mDefaultFont == mFonts.end()){
		throw std::runtime_error("Default Font not set");
	}
	return mDefaultFont->second;
}