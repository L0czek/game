#include "laserFly.hpp"
#include "Box2D/Box2D.h"
#include "rayCast.hpp"
#include "player.hpp"
#include "collisions.hpp"
#include "random.hpp"

struct DamageVisitor :public EntityVisitor{
	float mDamage;
	DamageVisitor(float mDamage) :mDamage(mDamage) {}
	void visit(Player& entity) override{
		entity.takeDamage(mDamage);
	}
	void visit(Platform& entity) override{}
	void visit(Lift&) override{}
	void visit(LaserFly& entity) override{}
	void apply(ContactInterface& interface) {
		interface.accept(*this);
	}
};

LaserFly::LaserFly(b2World& world,const sf::Vector2u& position) :mWorld(world){
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;
	bodyDef.position.Set( Scale::SFML2Box( position.x ) , Scale::SFML2Box( position.y ) );	
	bodyDef.angle = 0;
	mSprite = std::make_unique<SingleStringSprite>(world,bodyDef);
	mSprite->setSpriteSheet(SingleStringSprite::imageContainer::getInstance().getSpriteSheets("laserFly"));
	mLaserTexture = SingleStringSprite::imageContainer::getInstance().getSingleTexture("laser");
	auto size = mLaserTexture->getSize();
	mLaserSingleFrame = sf::Vector2u{ size.x / 6, size.y / 4 };
	mLaserSprite.setTexture(*mLaserTexture);
	setHealth(totalHealth());
	mLeftGroundSensor = std::make_unique<ContactSensor<Platform,Lift>>(*mSprite->getBody(), sf::Vector2i{-50, 50}, sf::Vector2i{5, 5});
	mRightGroundSensor = std::make_unique<ContactSensor<Platform,Lift>>(*mSprite->getBody(), sf::Vector2i{+50, 50}, sf::Vector2i{5, 5});
	mLeftRearSensor = std::make_unique<ContactSensor<Platform,Lift>>(*mSprite->getBody(), sf::Vector2i{-50, 0}, sf::Vector2i{10,30});
	mRightRearSensor = std::make_unique<ContactSensor<Platform,Lift>>(*mSprite->getBody(), sf::Vector2i{+50, 0}, sf::Vector2i{10,30});
	mGroundSensor = std::make_unique<ContactSensor<Platform,Lift,Player>>(*mSprite->getBody(), sf::Vector2i{0, 50}, sf::Vector2i{ 35, 5});
	mGroundSensor->onContact<Player>([](Player& player){
		player.takeDamage(1000);
	});
	mHealthBar.setColor(sf::Color::Red, sf::Color::Red);
}
void LaserFly::load() {
	mSprite->setLastChunk("walk", sf::Vector2u{3, 3});
	mSprite->setLastChunk("walk_change_direction", sf::Vector2u{3,3});
	mSprite->setFixtureData(1, 0.2, 0.2);
	mSprite->setImage("walk");
	mSprite->resize(100,100);
	mSprite->setOriginToMiddle();
    mSprite->setUserData(this);
    mSprite->runAnimation(0.05, true);
    mSprite->getBody()->SetLinearVelocity(b2Vec2{-1, 0});
	setCollisionFilter(mSprite->getFixture(), ObjectType::Enemy, collideWithAllExcept(ObjectType::Enemy));
    mSprite->update();
    mState = State::GoLeft;
}
void LaserFly::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	if(mState == State::LaserAttackForward){
		target.draw(mLaserSprite, states);
	}
	target.draw(*mSprite, states);
	if(mState != State::Dead){
		target.draw(mHealthBar, states);
	}
}
bool LaserFly::checkForPlayerInRange(const sf::Vector2i& direction) {
	struct PlayerTypeCheck :public TypeCheckVisitor{
		void visit(Player&) override{ correct(); }
	}visitor;
	auto startPoint = mSprite->getPosition() + convert<float>(direction * sf::Vector2i{50, 0});
	auto endPoint = startPoint + convert<float>(direction * 400);
	//std::cout << "fly : " << startPoint.x << " " << startPoint.y << "   " << endPoint.x << " " << endPoint.y <<"\n";
	auto ray = RayCast::findNearst(mWorld, startPoint, endPoint);
	if(ray){
		ContactInterface& interface = *reinterpret_cast<ContactInterface*>(ray->mTarget->GetBody()->GetUserData());
		interface.accept(visitor);
		if(visitor.getResult()){
			mDistanceToPlayer = ray->mDistance;
			return true;
		}
	}
	return false;
}
void LaserFly::jump(const sf::Vector2f& start,const sf::Vector2f& destination) {
	mSprite->setImage("jump");
	mSprite->runAnimation(0.05, false);
	mState = State::Jump;
	auto dx = Scale::SFML2Box(destination.x - start.x);
	auto vx = (dx / 1.2f)*5.0f;
	auto vy = -9.81f*1.2f/2.0f;
	mSprite->getBody()->SetLinearVelocity(b2Vec2{vx, vy});
}
void LaserFly::attackForward() {
	mSprite->setImage("laser_attack_forward");
	mSprite->runAnimation(0.05, false);
	mState = State::LaserAttackForward;
	mLaserClock.restart();
	auto scale = mSprite->getScale();
	mLaserSprite.setScale(scale);
	mLaserSpriteCordinates = {0,0};
	DamageVisitor visitor{RandomGenerator::getInstance().getNumber(25.f, 50.f)};
	sf::Vector2f start = mSprite->getPosition();
	sf::Vector2f end = start;
	if(mSprite->getScale().x < 0){
		end += sf::Vector2f{1,0} * 400;
	}else{
		end += sf::Vector2f{-1,0} * 400;
	}
	RayCast::forAllInRange(mWorld, start, end, std::bind(&DamageVisitor::apply, &visitor, std::placeholders::_1));
}
void LaserFly::update() {
	if(mState == State::Dead){
		mSprite->update();
		return;
	}
	if(mSprite->animationIsRunning()){
		mSprite->animationStep();
	}
	b2Body& body = *mSprite->getBody();
	auto velocity = body.GetLinearVelocity();
	float vx = 0;
	auto scale = mSprite->getScale();
	if(mState != State::Death && getHealth() <= 0){
		mSprite->setImage("death");
		mSprite->runAnimation(0.05, false);
		setCollisionFilter(mSprite->getFixture(), ObjectType::Enemy, collideWith(ObjectType::Platform));
		mGroundSensor->turnOff();
		mLeftGroundSensor->turnOff();
		mRightGroundSensor->turnOff();
		mLeftRearSensor->turnOff();
		mRightRearSensor->turnOff();
		mState = State::Death;
	}
	switch(mState){
		case State::GoRight: 
			if(mGroundSensor->isInContact()){
				if(!mRightGroundSensor->isInContact() || mRightRearSensor->isInContact()){
					turnLeft();
				}
				if(checkForPlayerInRange(sf::Vector2i{1,0})){
					if(mDistanceToPlayer > 50){
						auto position = mSprite->getPosition();
						jump(position, sf::Vector2f{position.x + mDistanceToPlayer, position.y});
					}else{
						attackForward();
					}
					break;
				}
			}			
			velocity.x = 1;
			body.SetLinearVelocity(velocity);
			break;	
		case State::GoLeft:
			velocity.x = -1;
			if(mGroundSensor->isInContact()){
				if(!mLeftGroundSensor->isInContact() || mLeftRearSensor->isInContact()){
					turnRight();	
				}
				if(checkForPlayerInRange(sf::Vector2i{-1,0})){
					if(mDistanceToPlayer > 50){
						auto position = mSprite->getPosition();
						jump(position, sf::Vector2f{position.x - mDistanceToPlayer, position.y});
					}else{
						attackForward();
					}
					break;
				}
			}	
			body.SetLinearVelocity(velocity);
			break;
		case State::TurnLeft:
			if(!mSprite->animationIsRunning()){
				mSprite->setImage("walk");
				mSprite->runAnimation(0.05, true);
				mState = State::GoLeft;
			}
			velocity.x = 0;
			body.SetLinearVelocity(velocity);
			break;
		case State::TurnRight:
			if(!mSprite->animationIsRunning()){
				mSprite->setImage("walk");
				mSprite->runAnimation(0.05, true);
				mState = State::GoRight;
			}
			velocity.x = 0;
			body.SetLinearVelocity(velocity);
			break;
		case State::LaserAttackForward:
			if(!mSprite->animationIsRunning()){
				mSprite->setImage("walk");
				mSprite->runAnimation(0.05, true);
				if(mSprite->getScale().x > 0){
					mState = State::GoLeft;
				}else{
					mState = State::GoRight;
				}
			}
			if(mLaserClock.getElapsedTime().asSeconds() >= 0.125){
				mLaserClock.restart();
				mLaserSpriteCordinates.x++;
				if(mLaserSpriteCordinates.x >= 6){
					mLaserSpriteCordinates.x = 0;					
					mLaserSpriteCordinates.y++;
					if(mLaserSpriteCordinates.y >= 4){
						mLaserSpriteCordinates.y = 0;
					}
				}
				if(mSprite->getScale().x < 0){
					mLaserSprite.setPosition(mSprite->getPosition() + sf::Vector2f{650, -50});
				}else{
					mLaserSprite.setPosition(mSprite->getPosition() + sf::Vector2f{-650, -50});
				}
				mLaserSprite.setTextureRect(sf::IntRect{ convert<int>(mLaserSpriteCordinates * mLaserSingleFrame), convert<int>(mLaserSingleFrame)});
			}
			velocity.x = 0;
			body.SetLinearVelocity(velocity);
			break;
		case State::Jump:
			if(!mSprite->animationIsRunning()){
				mSprite->setImage("walk");
				mSprite->runAnimation(0.05, true);
				if(mSprite->getScale().x > 0){
					mState = State::GoLeft;
				}else{
					mState = State::GoRight;
				}
			}
			break;
		case State::Death: 
			if(!mSprite->animationIsRunning()){
				mState = State::Dead;
			}
			break;
		case State::Dead: 

			break;
	}
	mHealthBar.update(mSprite->getPosition(), getHealth() / totalHealth());
	mSprite->update();
}
void LaserFly::hitFrom(const sf::Vector2f& position) {
	if(mSprite->getPosition().x > position.x){
		if(mState == State::GoRight){
			turnLeft();
		}
	}else{
		if(mState == State::GoLeft){
			turnRight();
		}
	}
}

void LaserFly::turnRight() {
	auto scale = mSprite->getScale();
	auto velocity = mSprite->getBody()->GetLinearVelocity();
	scale.x = -scale.x;
	mSprite->setScale(scale);
	mSprite->setImage("walk_change_direction");
	mSprite->runAnimation(0.05, false);
	velocity.x = 0;
	mSprite->getBody()->SetLinearVelocity(velocity);
	mState = State::TurnRight;
}
void LaserFly::turnLeft() {
	auto scale = mSprite->getScale();
	auto velocity = mSprite->getBody()->GetLinearVelocity();
	scale.x = std::abs(scale.x);
	mSprite->setScale(scale);
	mSprite->setImage("walk_change_direction");
	mSprite->runAnimation(0.05, false);
	velocity.x = 0;
	mSprite->getBody()->SetLinearVelocity(velocity);
	mState = State::TurnLeft;
}
EnemySyncPacket LaserFly::getState() const{
	EnemySyncPacket packet;
	packet.mId = getNetworkId();
	packet.mPosition = mSprite->getBody()->GetPosition();
	packet.mVelocity = mSprite->getBody()->GetLinearVelocity();
	packet.mImage = mSprite->getSelectedImageId();
	packet.mSpriteCordinates = mSprite->getAnimationCordinates();
	packet.mWeaponSpriteConrdinates = mLaserSpriteCordinates;
	packet.mScale = mSprite->getScale();
	packet.mHealth = getHealth();
	packet.mState = static_cast<sf::Uint64>(mState);
	return packet;
}
void LaserFly::setState(const EnemySyncPacket& packet) {
	mState = static_cast<State>(packet.mState);
	mSprite->getBody()->SetLinearVelocity(packet.mVelocity);
#ifdef __SYNC_POSITION__
	mSprite->getBody()->SetTransform(packet.mPosition, mSprite->getBody()->GetAngle());
#endif
	if(!mSprite->isImageSelected(packet.mImage)){
		mSprite->setImage(packet.mImage);
	}
	if(mSprite->getAnimationCordinates() != packet.mSpriteCordinates){
		mSprite->setChunk(packet.mSpriteCordinates.x, packet.mSpriteCordinates.y);
	}
	mLaserSpriteCordinates = packet.mWeaponSpriteConrdinates;
	mSprite->setScale(packet.mScale);
	mLaserSprite.setScale(packet.mScale);
	setHealth(packet.mHealth);
	mSprite->update();
	if(mSprite->getScale().x < 0){
		mLaserSprite.setPosition(mSprite->getPosition() + sf::Vector2f{650, -50});
	}else{
		mLaserSprite.setPosition(mSprite->getPosition() + sf::Vector2f{-650, -50});
	}
	mLaserSprite.setTextureRect(sf::IntRect{ convert<int>(mLaserSpriteCordinates * mLaserSingleFrame), convert<int>(mLaserSingleFrame)});
	mHealthBar.update(mSprite->getPosition(), getHealth() / totalHealth());
	if(mState == State::Death && !mNetworkDeath){
		setCollisionFilter(mSprite->getFixture(), ObjectType::Enemy, collideWith(ObjectType::Platform));
		mGroundSensor->turnOff();
		mLeftGroundSensor->turnOff();
		mRightGroundSensor->turnOff();
		mLeftRearSensor->turnOff();
		mRightRearSensor->turnOff();
		mNetworkDeath = true;
	}
}