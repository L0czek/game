#include "progressBar.hpp"
#include <iostream>

ProgressBar::ProgressBar(const sf::Vector2f& offset,const sf::Vector2f& size) :mOffset(offset), mSize(size){
	mOutline.setOutlineThickness(2);
}
void ProgressBar::update(const sf::Vector2f& position,float duty) {
	setPosition(position);
	update(duty);
}
void ProgressBar::setColor(const sf::Color& inside,const sf::Color& outline) {
	mInside.setFillColor(inside);
	mOutline.setOutlineColor(outline);
	mOutline.setFillColor(sf::Color::Transparent);
	mInside.setOutlineColor(sf::Color::Transparent);
}
void ProgressBar::draw(sf::RenderTarget& target,sf::RenderStates states) const{
	target.draw(mInside, states);
	target.draw(mOutline, states);
}
void ProgressBar::update(float duty) {
	auto size_x = mSize.x * duty;
	if(size_x < 0){
		size_x = 0;
	}
	mInside.setSize( sf::Vector2f{ size_x, mSize.y});
}
void ProgressBar::setPosition(const sf::Vector2f& position) {
	auto newPosition = position + mOffset;
	mInside.setPosition(newPosition);
	mOutline.setPosition(newPosition);
}
void ProgressBar::setSize(const sf::Vector2f& size) {
	mSize = size;
	mOutline.setSize(mSize);
}