#include "textInput.hpp"
#include "common.hpp"
#include "events.hpp"

std::string TextInput::getString() {
	return mString;
}
void TextInput::addLetter(char letter) {
	if(letter == '\x08'){
		backspace();
	}else if(mString.length() < mMaxSize){
		mString += letter;
	}
	mText.setString(sf::String(mString));
	auto bounds = mText.getGlobalBounds();
	mText.setOrigin(sf::Vector2f{bounds.width, bounds.height} / 2);
}
void TextInput::backspace() {
	if(mString.length() > 0)
		mString.erase(--mString.end());
}
void TextInput::setColor(const sf::Color& color) {
	mShape.setFillColor(color);
}
void TextInput::setFont(const sf::Font& font,unsigned int fontSize,const sf::Color& color) {
	mText.setFont(font);
	mText.setFillColor(color);
	mText.setCharacterSize(fontSize);
}
void TextInput::update() {

}
void TextInput::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mShape, states);
	target.draw(mText, states);
}
void TextInput::setBounds(const sf::Vector2i& position,const sf::Vector2i& size) {
	mShape.setSize(convert<float>(size));
	mShape.setOrigin(convert<float>(size) / 2);
	mShape.setPosition(convert<float>(position));
	mText.setPosition(convert<float>(position) - sf::Vector2f{0, 9});
}
TextInput::TextInput(){

}
void TextInput::registerCallback() {
	GlobalEvents::getInstance().clearText();
	GlobalEvents::getInstance().addTextEventCallback(std::bind(&TextInput::addLetter, this, std::placeholders::_1));
}
void TextInput::setMaxSize(std::size_t size) {
	mMaxSize = size;
}