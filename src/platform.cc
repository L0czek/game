#include "platform.hpp"
#include "collisions.hpp"

Platform::Platform(b2World& world,const sf::Vector2u& position,const sf::Vector2u& size,const std::shared_ptr<sf::Texture>& texture,float friction) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set( Scale::SFML2Box( position.x ), Scale::SFML2Box( position.y ) );
	bodyDef.angle = 0;
    mSprite = std::make_unique<SingleStringSprite>(world,bodyDef,texture);
    mSprite->setSpriteSheet();
    mSprite->setFixtureData(friction, 0.01, 0.2);
    mSprite->resize(size.x, size.y);
    mSprite->setUserData(this);
    mSprite->setOriginToMiddle();
    setCollisionFilter(mSprite->getFixture(), ObjectType::Platform, collideWithAll());
    mSprite->update();

}
void Platform::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(*mSprite,states);
}
void Platform::update() {
	mSprite->update();
}