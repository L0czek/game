#include "map.hpp"
#include "player.hpp"
#include "network.hpp"

Map::Map(const sf::FloatRect& initialViewport,const b2Vec2& gravity) :mWorld(gravity), mView(initialViewport) {
	mWorld.SetContactListener(&mContactListener);
}
void Map::createPlayer(const sf::Vector2u& position) {
	mPlayer = std::make_unique<Player>(mWorld,position);
}
void Map::destroyPlayer() {
	mPlayer.reset();
}

Map::iterator Map::addStaticEntity(std::unique_ptr<Entity> entity) {
	return std::get<0>(mStaticEntities.insert(std::move(entity)));
}
Map::dynamicIterator Map::addDynamicEntity(std::unique_ptr<DynamicEntity> entity) {
	return std::get<0>(mDynamicEntities.insert(std::move(entity)));
}
auto Map::addEnemy(std::unique_ptr<Enemy> entity) {
	return std::get<0>(mEnemies.insert(std::move(entity)));
}

void Map::removeStaticEntity(iterator it) {
	mStaticEntities.erase(it);
}
void Map::removeDynamicEntity(dynamicIterator it) {
	mDynamicEntities.erase(it);
}
//void Map::removeEnemy(livingIterator it) {
//	mEnemies.erase(it);
//}

void Map::clearStaticEntity() {
	mStaticEntities.clear();
}
void Map::clearDynamicEntity() {
	mDynamicEntities.clear();
}
void Map::clearEnemy() {
	mEnemies.clear();
}
void Map::clearAll() {
	clearStaticEntity();
	clearDynamicEntity();
	clearEnemy();
	destroyPlayer();
	mNetworkPlayers.clear();
	mPlayerScripts.clear();
}
void Map::addNetworkPlayer(const std::string& name, const sf::Vector2u& pos,bool onHost) {
		auto &it = mNetworkPlayers.emplace_back(std::make_unique<Player>(mWorld,pos));
		it->setNick(name);
		it->setNetworkMode(onHost);
		it->load();
}
void Map::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mBackGround, states);
	for(const auto &i : mStaticEntities){
		target.draw(*i, states);
	}
	for(const auto &i : mDynamicEntities){
		target.draw(*i, states);
	}
	for(const auto &i : mEnemies){
		target.draw(*i, states);
	}
	if(mPlayer){
		target.draw(*mPlayer, states);
	}
	for(const auto &i : mNetworkPlayers){
		target.draw(*i, states);
	}
}

void Map::update() {
	mWorld.Step(mClock.restart().asSeconds(), velocityIterations, positionIterations);
	for(const auto &i : mStaticEntities){
		i->update();
	}
	for(const auto &i : mDynamicEntities){
		i->update();
	}
	for(const auto &i : mEnemies){
		i->update();
	}
	updatePlayer();
}

void Map::load() {
	for(const auto &i : mStaticEntities){
		i->load();
	}
	for(const auto &i : mDynamicEntities){
		i->load();
	}
	for(const auto &i : mEnemies){
		i->load();
	}
	if(mPlayer){
		mPlayer->load();
	}
}
void Map::updateWindow(sf::RenderWindow& window) const{
	window.setView(mView);
}
void Map::platformsFromString(const std::string& content,char chunk,const sf::Vector2u& chunkSize,const std::shared_ptr<sf::Texture>& texture) {
	std::size_t cnt = 0;
	sf::Vector2u position;
	std::size_t x = 0;
	std::size_t y = 0;
	for(std::size_t i=0; i < content.length(); ++i){
		if(content[i] == chunk){
			if(cnt == 0){
				position.x = chunkSize.x * x;
				position.y = chunkSize.y * y + chunkSize.y - 10;
			}
			cnt++;
		}else{
			if(cnt > 0){
				auto end = sf::Vector2u{ static_cast<unsigned int>(x), static_cast<unsigned int>(y)} * chunkSize;
				end.y += chunkSize.y - 10;
				auto size = end - position;
				auto pos = end - size / 2;
				size.y = 10;
				addStaticEntity<Platform>(pos, size, texture);
				cnt = 0;
			}
			if(content[i] == '\n'){
				x = 0;
				y++;
				continue;
			}
		}
		x++;
	}
}
void Map::removeStaticEntityScript(scriptIterator it) {
	mStaticEntitiesScripts.erase(it);
}
//void Map::removeDynamicEntityScript(scriptIterator it) {
//	mStaticDynamicScripts.erase(it);
//}
//void Map::removeEnemyScript(scriptIterator it) {
//	mEnemiesScripts.erase(it);
//}
void Map::removePlayerScript(playerScriptIterator it) {
	mPlayerScripts.erase(it);
}
const int Map::velocityIterations = 8;   //how strongly to correct velocity
const int Map::positionIterations = 3;
bool Map::areAllEnemiesDead() const{
	for(const auto &i : mEnemies){
		if(!i->isDead()){
			return false;
		}
	}
	return true;
}
void Map::updateNetworkState(NetworkServer& server) {
	server.setDynamicEntitiesState(mDynamicEntities);
	server.setEnemiesState(mEnemies);
	if(mPlayer)
		server.setPlayerState(mPlayer);
	server.getNetworkPlayersState(mNetworkPlayers);
	server.sync();
}
void Map::updateNetworkState(NetworkClient& client) {
	mWorld.Step(mClock.restart().asSeconds(), velocityIterations, positionIterations);
	client.getDynamicEntitiesState(mDynamicEntities);
	client.getEnemiesState(mEnemies);
	if(mPlayer)
		client.setPlayerState(mPlayer);
	client.getNetworkPlayersState(mNetworkPlayers);
	updatePlayer();
}
void Map::updatePlayer() {
	if(mPlayer){
		mPlayer->update();
		if(mScriptsEnabled){
			auto current = mPlayerScripts.begin();
			auto next = std::next(current);
			for(; current != mPlayerScripts.end(); ){
				if(current->first(*this,mPlayer)){
					current->second(*this, mPlayer);
				}
				current = next;
				if(next != mPlayerScripts.end()){
					next = std::next(next);
				}
			}	
		}
		auto pos = mPlayer->getPosition();
		auto view = mView.getCenter();
		view.y -= mView.getSize().y / 2;
		int y = pos.y - view.y;
		if(y > mView.getSize().y){
			y = mView.getSize().y;
		}else if(y < 0){
			y = -mView.getSize().y;
		}else{
			y = 0;
		}
		//mView.move(pos.x - view.x, y);
		mView.setCenter(pos);
	}
}
bool Map::areAllPlayersDead() const{
	for(const auto &i : mNetworkPlayers){
		if(!i->isDead()){
			return false;
		}
	}
	if(mPlayer && !mPlayer->isDead()){
		return false;
	}
	return true;
}