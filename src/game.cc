#include "game.hpp"
#include <chrono>
#include <thread>
#include "events.hpp"

Game::Game(const GameConfig& config) :mConfig(config), mWindow(sf::VideoMode(config.mWindowSize.x, config.mWindowSize.y), "SFML") {
	mWindow.setFramerateLimit(config.mFrameLimit);
	mBsod.setTexture(*SingleStringSprite::imageContainer::getInstance().getSingleTexture("bsod"));
}

void Game::loop() {
	if(!mMap || !mInitialScreen || !mGameOverScreen || !mGameWinScreen || mLoadingScreens.size() == 0){
		throw std::runtime_error("Game lacks initialization");
	}
	while(mWindow.isOpen()){
		resolvEvents();
		mWindow.clear();
		switch(mDrawingState){
			case DrawingState::None: break;
			case DrawingState::MapHost:
				mMap->update();
				mMap->updateWindow(mWindow);
				mMap->updateNetworkState(*mNetworkServer);
				mWindow.draw(*mMap);
				break;
			case DrawingState::MapClient:
				mMap->updateNetworkState(*mNetworkClient);
				mMap->updateWindow(mWindow);
				mWindow.draw(*mMap);
				break;
			case DrawingState::HostScreen:
				mGameMultiHostScreen->update();
				mWindow.draw(*mGameMultiHostScreen);
				break;
			case DrawingState::ClientScreen:
				mGameMultiClientScreen->update();
				mWindow.draw(*mGameMultiClientScreen);
				break;
			case DrawingState::InitialScreen: 
				mInitialScreen->update();
				mWindow.draw(*mInitialScreen);
				break;
			case DrawingState::Map: 
				mMap->update();
				mMap->updateWindow(mWindow);
				mWindow.draw(*mMap);
				break;
			case DrawingState::GameOverScreen: 
				mMap->update();
				mGameOverScreen->update();
				if(isInNetworkMode()){
					if(isHost()){
						mMap->updateNetworkState(*mNetworkServer);

					}else{
						mMap->updateNetworkState(*mNetworkClient);
					}
				}
				mWindow.draw(*mMap);
				mWindow.draw(*mGameOverScreen);
				break;
			case DrawingState::GameWinScreen:
				mGameWinScreen->update();
				mWindow.draw(*mMap);
				mWindow.draw(*mGameWinScreen);
				break;
			case DrawingState::LoadingScreen:
				mLoadingScreens[mLoadingScreenToDisplay]->update();
				mWindow.draw(*mLoadingScreens[mLoadingScreenToDisplay]);
				break;
		}
		mWindow.display();
	}
}
void Game::loadLevel(std::size_t levelIndex,std::size_t loadingScreenIndex) {
	if(mLevels.size() <= levelIndex){
		throw std::runtime_error("Error no such level");
	}
	if(mLoadingScreens.size() <= loadingScreenIndex){
		throw std::runtime_error("Error no such loading screen");	
	}
	mLoadingScreenToDisplay = loadingScreenIndex;
	mSelectedLevel = levelIndex;
	Level& level = *mLevels[levelIndex];
	mLoadingScreens[loadingScreenIndex]->create();
	level.load(std::bind(&LoadingScreen::updateProgress, mLoadingScreens[loadingScreenIndex].get(), std::placeholders::_1));
	if(!mMap){
		mMap = std::make_unique<Map>(mConfig.mInitialViewport, mConfig.mGravity);
	}
	mMap->clearAll();
	level(*mMap);
}
Map& Game::getMap() {
	if(!mMap){
		throw std::runtime_error("Erorr map wasnt allocated");
	}
	return *mMap;
}
void Game::gameOver(Map& map,const std::unique_ptr<Player>& player) {
	//map.disableScripts();
	map.removePlayerScript(mGameOverScript);
	mGameOverScreen->create(); 
	mDrawingState = DrawingState::GameOverScreen; 
	mLevels[mSelectedLevel]->stop();
}	
void Game::gameWin(Map& map,const std::unique_ptr<Player>& player) { 
	//map.disableScripts();
	map.removePlayerScript(mGameWinScript);
	mGameWinScreen->create(); 
	mDrawingState = DrawingState::GameWinScreen; 
	mLevels[mSelectedLevel]->stop();
}
void Game::startGame() {
	mMap->clearPlayerScripts();
	mMap->enableScripts();
	setGameOverCondition(mLevels[mSelectedLevel]->getGameOverCondition());
	setGameWinCondition(mLevels[mSelectedLevel]->getGameWinCondition());
	mDrawingState = DrawingState::Map;
	mLevels[mSelectedLevel]->start();
}
bool Game::isNextLevel() {
	return (mSelectedLevel + 1) < mLevels.size();
}
void Game::nextLevel() {
	mLevels[mSelectedLevel]->stop();
	if(!isNextLevel()){
		throw std::runtime_error("Error next level doesnt exist");
	}
	mSelectedLevel++;
	if(isInNetworkMode()){
		if(isHost()){
			NextLevel packet;
			mNetworkServer->sendToAll(packet);
			setupLevelHost(mSelectedLevel, mLoadingScreenToDisplay);
		}else{
			setupLevelClient(mSelectedLevel, mLoadingScreenToDisplay);
		}
	}else{
		setupLevelSingle(mSelectedLevel, mLoadingScreenToDisplay);
	}
}
void Game::bsod() {
	using namespace std::chrono_literals;
	mWindow.clear();
	mWindow.draw(mBsod);
	mWindow.display();
	while(mWindow.isOpen()){
		sf::Event event;
		while(mWindow.pollEvent(event)){
			if(event.type == sf::Event::Closed){
				mWindow.close();
			}
		}
		std::this_thread::sleep_for(1s);
	}
}
Game::~Game() {
	if(mNetworkServer){
		mNetworkServer->stopAccepting();
	}
}
void Game::resolvEvents() {
	sf::Event event;
	while(mWindow.pollEvent(event)){
		if(event.type == sf::Event::Closed){
			mWindow.close();
		}else if(event.type == sf::Event::TextEntered){
			if(event.text.unicode < 0x7f){
				GlobalEvents::getInstance().notifyText(static_cast<char>(event.text.unicode));
			}
		}
	}
}
void Game::multiHostLoop() {
	mDrawingState = DrawingState::MapHost;
}
void Game::multiClientLoop() {
	mDrawingState = DrawingState::MapClient;
}
void Game::setupLevelHost(std::size_t level, std::size_t loadingScreen) {
	mNetworkServer->stopSync();
	LoadLevel packet;
	mNetworkServer->sendToAll(packet);
	loadLevel(level, loadingScreen);
	mNetworkServer->waitForClientsToLoad();
	auto position = mMap->getSpawnPoint();
	PlayerList list;
	list.mPlayers.emplace_back(std::make_pair(mNick, position));
	mMap->createPlayer(convert<unsigned int>(position));
	mMap->getPlayer()->setNick(mNick);
	mMap->getPlayer()->setNetworkMode(true);
	mMap->load();
	for(auto &i : mNetworkServer->getClients()){
		PlayerSet packet;
		packet.mPosition = mMap->getSpawnPoint();
		list.mPlayers.emplace_back(std::make_pair(i.first, packet.mPosition));
		mMap->addNetworkPlayer(i.first, packet.mPosition, true);
		mNetworkServer->send(i.first, packet);
	}
	StartGame startPacket;
	mNetworkServer->sendToAll(list);
	mNetworkServer->sendToAll(startPacket);
	mNetworkServer->startSync();
	startGame();
	multiHostLoop();
}
void Game::setupLevelClient(std::size_t level, std::size_t loadingScreen) {
	mNetworkClient->stopSync();
	loadLevel(level, loadingScreen);
	LevelLoaded packet;
	mNetworkClient->send(packet);
	auto position = mNetworkClient->getPlayerPosition();
	mMap->createPlayer(convert<unsigned int>(position));
	mMap->getPlayer()->setNick(mNick);
	mMap->getPlayer()->setNetworkMode(false);
	auto playersPacket = mNetworkClient->getPlayerList();
	for(const auto &i : playersPacket.mPlayers){
		if(i.first != mNick){
			mMap->addNetworkPlayer(i.first, i.second, false);
		}
	}
	mMap->load();
	mNetworkClient->waitForServerToLoad();
	mNetworkClient->startSync();
	startGame();
	multiClientLoop();
}
void Game::setupLevelSingle(std::size_t level, std::size_t loadingScreen) {
	mNetworkMode = NetworkMode::None;
	loadLevel(level, loadingScreen);
	auto position = mMap->getSpawnPoint();
	mMap->createPlayer(convert<unsigned int>(position));
	mMap->load();
	mMap->getPlayer()->setNick(mNick);
	mMap->getPlayer()->setNetworkMode(true);
	startGame();
}