#include "contactListener.hpp"

#include "Box2D/Box2D.h"
#include "entity.hpp"

#include <vector>

void ContactListener::BeginContact(b2Contact* contact) {
	b2Fixture& a = *contact->GetFixtureA();
	b2Fixture& b = *contact->GetFixtureB();
	std::vector<ContactInterface*> ptrs;
	ptrs.reserve(4);
	if(a.GetUserData()){
		ptrs.emplace_back(reinterpret_cast<ContactInterface*>(a.GetUserData()));
	}
	if(a.GetBody()->GetUserData()){
		ptrs.emplace_back(reinterpret_cast<ContactInterface*>(a.GetBody()->GetUserData()));	
	}
	if(b.GetUserData()){
		ptrs.emplace_back(reinterpret_cast<ContactInterface*>(b.GetUserData()));
	}
	if(b.GetBody()->GetUserData()){
		ptrs.emplace_back(reinterpret_cast<ContactInterface*>(b.GetBody()->GetUserData()));	
	}
	for(std::size_t i=0; i < ptrs.size(); ++i){
		for(std::size_t j=i+1; j < ptrs.size(); ++j){
			ptrs[i]->BeginContact(*ptrs[j]);
			ptrs[j]->BeginContact(*ptrs[i]);
		}
	}
}

void ContactListener::EndContact(b2Contact* contact) {
	b2Fixture& a = *contact->GetFixtureA();
	b2Fixture& b = *contact->GetFixtureB();
	std::vector<ContactInterface*> ptrs;
	ptrs.reserve(4);
	if(a.GetUserData()){
		ptrs.emplace_back(reinterpret_cast<ContactInterface*>(a.GetUserData()));
	}
	if(a.GetBody()->GetUserData()){
		ptrs.emplace_back(reinterpret_cast<ContactInterface*>(a.GetBody()->GetUserData()));	
	}
	if(b.GetUserData()){
		ptrs.emplace_back(reinterpret_cast<ContactInterface*>(b.GetUserData()));
	}
	if(b.GetBody()->GetUserData()){
		ptrs.emplace_back(reinterpret_cast<ContactInterface*>(b.GetBody()->GetUserData()));	
	}
	for(std::size_t i=0; i < ptrs.size(); ++i){
		for(std::size_t j=i+1; j < ptrs.size(); ++j){
			ptrs[i]->EndContact(*ptrs[j]);
			ptrs[j]->EndContact(*ptrs[i]);
		}
	}
}
