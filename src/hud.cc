#include "hud.hpp"
#include "common.hpp"

HUD::HUD(const std::string& fontName) {
	if(!mFont.loadFromFile(fontName)){
		throw std::runtime_error("Error can not load font.");
	}
	mHealth.setFont(mFont);
	mAmmo.setFont(mFont);
	mHealth.setFillColor(sf::Color::Yellow);
	mAmmo.setFillColor(sf::Color::Yellow);
	mHealth.setString(L"mHealth");
	mAmmo.setString(L"mAmmo");
	mHealth.setPosition(0,0);
	mAmmo.setPosition(0,560);
}

void HUD::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mHealth, states);
	target.draw(mAmmo, states);
}