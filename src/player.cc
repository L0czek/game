#include "player.hpp"
#include "collisions.hpp"
#include "fontContainer.hpp"

void Player::load() {
	mSprite->setFixtureData(1, 0.05, 0);
    mSprite->setImage("idle");
    mSprite->runAnimation(0.05, true);
    mSprite->resize(70,80);
    mSprite->setOriginToMiddle();
    mSprite->setUserData(this);
    mSprite->update();
    mState = State::Walk;
}


Player::Player(b2World& world,const sf::Vector2u& StartPosition) :mAK47(world), mBaseBall(world) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;
	bodyDef.position.Set( Scale::SFML2Box( StartPosition.x ) , Scale::SFML2Box( StartPosition.y ) );	
	bodyDef.angle = 0;
	mSprite = std::make_unique<MultiStringSprite>(world,bodyDef);
	mSprite->setSpriteSheet(SingleStringSprite::imageContainer::getInstance().getSpriteSheets("player"));
    b2PolygonShape shape;
    shape.SetAsBox(Scale::SFML2Box(40/2.0), Scale::SFML2Box(40/2.0), b2Vec2{0, Scale::SFML2Box(-20)}, 0);
    b2FixtureDef f;
    f.shape = &shape;
    f.density = 1;
	mSprite->addFixture(f);
	setCollisionFilter(mSprite->front(), ObjectType::Player, collideWithAll());
    shape.SetAsBox(Scale::SFML2Box(40/2.0), Scale::SFML2Box(40/2.0), b2Vec2{0, Scale::SFML2Box(+20)}, 0);
    mSprite->addFixture(f);
   	setCollisionFilter(mSprite->back(), ObjectType::Player, collideWithAll());
	mGroundSensor = std::make_unique<ContactSensor<Platform,Lift,LaserFly>>(*mSprite->getBody(), sf::Vector2i{0, 40}, sf::Vector2i{30, 5});
	setHealth(totalHealth());
	mHealthBar.setColor(sf::Color::Red, sf::Color::Red);
	mNick.setFillColor(sf::Color::Yellow);
	mNick.setFont(FontContainer::getInstance().getDefaultFont());
	mNick.setString(sf::String("nick"));
}
	
void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(*mSprite,states);
	target.draw(mNick, states);
	if(mState != State::Dead){
		target.draw(mHealthBar, states);
	}
}
void Player::update() {
	if(mState == State::Dead){
		mSprite->update();
		return;
	}
	b2Body & body = *mSprite->getBody();
	if(mSprite->animationIsRunning()){
		mSprite->animationStep();
	}
	auto speed = body.GetLinearVelocity();
	int vx = 0;
	sf::Vector2f scale;
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q)){
		mEquippedWeapon = Weapon::AK47;
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::E)){
		mEquippedWeapon = Weapon::BaseBall;
	}
	if(mState != State::Death && getHealth() <= 0){
		if(mEquippedWeapon == Weapon::AK47){
			mSprite->setImage("death");
		}else{
			mSprite->setImage("death_bat");
		}
		mSprite->runAnimation(0.05, false);
		mState = State::Death;
	}
	//std::cout << "player : " << mSprite->getPosition().x << " " << mSprite->getPosition().y << "\n";
	//std::cout << mGroundSensor->mGroundContact << "\n";
	switch(mState){
		case State::Walk:
			if(mGroundSensor->isInContact()){
				if(mEquippedWeapon == Weapon::AK47 && sf::Keyboard::isKeyPressed(sf::Keyboard::C) && HavePassed(0.05)){
					mSprite->setImage("crouch_start");
					mSprite->runAnimation(0.05, false);
					mState = State::CrouchStart;
					break;
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && HavePassed(0.05)){
					mSprite->setImage(resolveSpriteName("jump"));
					mSprite->runAnimation(0.02, false);
					body.ApplyLinearImpulse(b2Vec2{0, -2.5}, body.GetWorldCenter(), true);
					mState = State::Jump;
					break;
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
					vx += 2;
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
					vx -= 2; 
				}
				speed.x = vx;
				body.SetLinearVelocity(speed);
				scale = mSprite->getScale();
				if(vx * scale.x < 0){
					scale.x *= -1;
					mSprite->setScale(scale);
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::F)){
					if(vx == 0){
						if(mEquippedWeapon == Weapon::AK47){
							if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
								if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
									mSprite->setImage("shot_diagonal_up");
									if(mSprite->getScale().x > 0){
										attack(mSprite->getPosition(), sf::Vector2f{ 1, -1});
									}else{
										attack(mSprite->getPosition(), sf::Vector2f{ -1, -1});
									}
								}else{
									mSprite->setImage("shot_up");
									attack(mSprite->getPosition(), sf::Vector2f{0, -1});
								}
							}else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
								mSprite->setImage("shot_diagonal_down");
								if(mSprite->getScale().x > 0){
									attack(mSprite->getPosition(), sf::Vector2f{ 1, 1});
								}else{
									attack(mSprite->getPosition(), sf::Vector2f{ -1, 1});
								}
							}else{
								if(mSprite->getScale().x < 0){
									attack(mSprite->getPosition(), sf::Vector2f{-1, 0});
								}else{
									attack(mSprite->getPosition(), sf::Vector2f{ 1, 0});
								}
								mSprite->setImage(resolveSpriteName("attack_idle"));
							}
						}else{
							mSprite->setImage(resolveSpriteName("attack_idle"));
							if(mSprite->getScale().x > 0){
								attack(mSprite->getPosition(), sf::Vector2f{ 1, 0});
							}else{
								attack(mSprite->getPosition(), sf::Vector2f{ -1, 0});
							}
						}
						mSprite->runAnimation(0.01, false);
						mState = State::AttackIdle;	
						break;			
					}else{
						if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && mEquippedWeapon == Weapon::AK47){
							mSprite->setImage("run_shot_up");
							if(mSprite->getScale().x > 0){
								attack(mSprite->getPosition(), sf::Vector2f{ 1, -1});
							}else{
								attack(mSprite->getPosition(), sf::Vector2f{ -1, -1});
							}
						}else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && mEquippedWeapon == Weapon::AK47){
							mSprite->setImage("run_shot_down");
							if(mSprite->getScale().x > 0){
								attack(mSprite->getPosition(), sf::Vector2f{ 1, 1});
							}else{
								attack(mSprite->getPosition(), sf::Vector2f{ -1, 1});
							}
						}else{
							if(mEquippedWeapon == Weapon::AK47){
								if(mSprite->getScale().x < 0){
									attack(mSprite->getPosition(), sf::Vector2f{-1, 0});
								}else{
									attack(mSprite->getPosition(), sf::Vector2f{ 1, 0});
								}
							}else{
								if(mSprite->getScale().x > 0){
									attack(mSprite->getPosition(), sf::Vector2f{ 1, 0});
								}else{
									attack(mSprite->getPosition(), sf::Vector2f{ -1, 0});
								}
							}
							mSprite->setImage(resolveSpriteName("attack_run"));
						}
						mSprite->runAnimation(0.01,false);
						mState = State::AttackWalk;
						break;
					}
				}
			}
			if(vx != 0){
				if(mEquippedWeapon == Weapon::AK47 && sf::Keyboard::isKeyPressed(sf::Keyboard::R)){
					mSprite->setImage("run_reload");
					mSprite->runAnimation(0.02, false);
					mState = State::ReloadIdle;
					break;
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::X) && mEquippedWeapon == Weapon::AK47){
					mSprite->setImage("belly_sliding_start");
					mSprite->runAnimation(0.01, false);
					mState = State::SlideStart;
					break;
				}else{
					if(!mSprite->isImageSelected(resolveSpriteName("run"))){
						mSprite->setImage(resolveSpriteName("run"));
						mSprite->runAnimation(0.02, true);
					}
				}
			}else{
				if(mEquippedWeapon == Weapon::AK47 && sf::Keyboard::isKeyPressed(sf::Keyboard::R)){
					mSprite->setImage("reload");
					mSprite->runAnimation(0.02, false);
					mState = State::ReloadIdle;
					break;
				}
				if(!mSprite->isImageSelected(resolveSpriteName("idle"))){
					mSprite->setImage(resolveSpriteName("idle"));
					mSprite->runAnimation(0.05, true);
				}
			}
			break;
		case State::Jump:
			if(mGroundSensor->isInContact() && HavePassed(0.05)){
				mState = State::Walk;
				break;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::F)){
				mSprite->setImage(resolveSpriteName("attack_jump"));
				mSprite->runAnimation(0.01, false);
				if(mSprite->getScale().x > 0){
					attack(mSprite->getPosition(), sf::Vector2f{ 1, 0});
				}else{
					attack(mSprite->getPosition(), sf::Vector2f{ -1, 0});
				}
				mState = State::AttackJump;
				break;
			}
			break;
		case State::CrouchStart:
			if(!mSprite->animationIsRunning()){
				mSprite->setImage("crouch_idle");
				mSprite->runAnimation(0.05, true);
				mState = State::CrouchIdle;
			}
			break;
		case State::CrouchIdle:
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::R)){
				mSprite->setImage("crouch_reload");
				mSprite->runAnimation(0.02, false);
				mState = State::ReloadCrouch;
				break;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::C)){
				mSprite->setImage("stand_up");
				mSprite->runAnimation(0.01, false);
				mState = State::CrouchEnd;
			}
			break;
		case State::CrouchEnd:
			if(!mSprite->animationIsRunning()){
				mState = State::Walk;
			}
			break;
		case State::AttackIdle:
			if(!mSprite->animationIsRunning()){
				mState = State::Walk;
			}
			break;
		case State::AttackJump:
			if(!mSprite->animationIsRunning()){
				mState = State::Jump;
			}
			break;
		case State::AttackWalk:
			if(!mSprite->animationIsRunning()){
				mState = State::Walk;
			}
			break;
		case State::SlideStart:
			if(!mSprite->animationIsRunning()){
				mSprite->setImage("belly_sliding");
				mSprite->runAnimation(0.05, true);
				setCollisionFilter(mSprite->front(), ObjectType::Player, collideWithNone());
				mState = State::SlideIdle;
			}
			break;
		case State::SlideIdle:
			if(speed.x == 0 || sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){ // TODO fix for floats
				mSprite->setImage("stand_up");
				mSprite->runAnimation(0.01, false);
				mState = State::SlideEnd;
				break;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::F)){
				mSprite->setImage("belly_sliding_and_shot");
				mSprite->runAnimation(0.01, false);
				mState = State::SlideShoot;
				break;
			}
			break;
		case State::SlideShoot:
			if(!mSprite->animationIsRunning()){
				mSprite->setImage("belly_sliding");
				mSprite->runAnimation(0.05, true);
				mState = State::SlideIdle;
			}
			break;
		case State::SlideEnd:
			if(!mSprite->animationIsRunning()){
				setCollisionFilter(mSprite->front(), ObjectType::Player, collideWithAll());
				mState = State::Walk;
			}
			break;
		case State::ReloadIdle:
			if(!mSprite->animationIsRunning()){
				mState = State::Walk;
			}
			break;
		case State::ReloadCrouch:
			if(!mSprite->animationIsRunning()){
				mState = State::CrouchIdle;
			}
			break;
		case State::Death: 
			if(!mSprite->animationIsRunning()){
				mState = State::Dead;
				constexpr auto collisionMask = collideWith(ObjectType::Platform);
				setCollisionFilter(mSprite->front(), ObjectType::Player, collisionMask);
				setCollisionFilter(mSprite->back(), ObjectType::Player, collisionMask);
				mSprite->removeFixture(mSprite->begin());
			}
			break;
		case State::Dead: puts("dead"); break;
	}
	mSprite->update();
	mHealthBar.update(mSprite->getPosition(), getHealth() / totalHealth());
	mNick.setPosition(mSprite->getPosition() + sf::Vector2f{-50, -100});
}

bool Player::HavePassed(float seconds){
	if(mClock.getElapsedTime().asSeconds() >= seconds){
		mClock.restart();
		return true;
	}
	return false;
}
std::string Player::resolveSpriteName(const std::string& name) const{
	auto it = mSpriteTranslation[static_cast<std::size_t>(mEquippedWeapon)].find(name);
	if(it != mSpriteTranslation[static_cast<std::size_t>(mEquippedWeapon)].end()){
		return it->second;
	}
	return name;
}
std::array<std::map<std::string,std::string>, 2> Player::mSpriteTranslation = {
	std::map<std::string,std::string>{ // AK47
		{ "idle", "idle"},
		{ "run", "run"},
		{ "crouch_start", "crouch_start"},
		{"stand_up", "stand_up"},
		{"jump", "jump"},
		{"crouch_idle", "crouch_idle"},
		{"attack_idle", "shot"},
		{"attack_jump", "jump_shot"},
		{"attack_run", "run_shot"}

	},
	std::map<std::string,std::string>{ // BaseBall
		{"idle", "idle_bat"},
		{"run", "run_bat"},
		{"jump", "jump_bat"},
		{"attack_idle", "idle_bat_attack"},
		{"attack_jump", "jump_bat_attack"},
		{"attack_run", "run_bat_attack"}
	}
};
void Player::attack(const sf::Vector2f& position, const sf::Vector2f& direction) {
	if(!mNetworkMode){
		mAttackRequests.emplace_back(position, direction);
	}else{
		if(mEquippedWeapon == Weapon::AK47){
			mAK47.shoot(position, direction);
		}else{
			mBaseBall.attack(position, direction);
		}
	}
}
PlayerSyncPacket Player::getState() const{
	PlayerSyncPacket packet;
	packet.mId = getNetworkId();
	packet.mPosition = mSprite->getBody()->GetPosition();
	packet.mVelocity = mSprite->getBody()->GetLinearVelocity();
	packet.mImage = mSprite->getSelectedImageId();
	packet.mSpriteCordinates = mSprite->getAnimationCordinates();
	packet.mScale = mSprite->getScale();
	packet.mHealth = getHealth();
	packet.mEquippedWeapon = static_cast<sf::Uint64>(mEquippedWeapon);
	packet.mAttackRequests.splice(packet.mAttackRequests.begin(), std::move(mAttackRequests));
	return packet;
}
void Player::setState(const PlayerSyncPacket& packet) {
	mEquippedWeapon = static_cast<Weapon>(packet.mEquippedWeapon);
	mSprite->getBody()->SetLinearVelocity(packet.mVelocity);
#ifdef __SYNC_POSITION__
	mSprite->getBody()->SetTransform(packet.mPosition, mSprite->getBody()->GetAngle());
#endif
	if(!mSprite->isImageSelected(packet.mImage)){
		mSprite->setImage(packet.mImage);
	}
	if(mSprite->getAnimationCordinates() != packet.mSpriteCordinates){
		mSprite->setChunk(packet.mSpriteCordinates.x, packet.mSpriteCordinates.y);
	}
	mSprite->setScale(packet.mScale);
	if(packet.mChangeHealth)
		setHealth(packet.mHealth);
	mSprite->update();
	mHealthBar.update(mSprite->getPosition(), getHealth() / totalHealth());
	mNick.setPosition(mSprite->getPosition() + sf::Vector2f{-50, -100});
	if(mState == State::Dead){
		return;
	}
	if(getHealth() <= 0){
		if(mState != State::Death){
			//setCollisionFilter(mSprite->front(), ObjectType::Player, collideWith(ObjectType::Platform));
			mSprite->removeFixture(mSprite->begin());
			setCollisionFilter(mSprite->back(), ObjectType::Player, collideWith(ObjectType::Platform));
			mState = State::Death;
		}
	}
}
void Player::quickUpdate() {
	mSprite->update();
	mHealthBar.update(mSprite->getPosition(), getHealth() / totalHealth());
	mNick.setPosition(mSprite->getPosition() + sf::Vector2f{-50, -100});
}