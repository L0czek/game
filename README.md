# Game



## Compiling
```
sudo apt update
sudo apt install libsfml-dev
git clone https://gitlab.com/L0czek/game.git
cd game
mkdir build
cd build
cmake ..
make
```

## To Run (inside build directory)
```
./bin/game
```

## To Clean
```
make clean
```



