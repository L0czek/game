#pragma once

#include <SFML/Graphics.hpp>
#include <map>

class FontContainer{
	std::map<std::string, sf::Font> mFonts;
	std::map<std::string, sf::Font>::iterator mDefaultFont;
	FontContainer() = default;
public:
	static FontContainer& getInstance();
	void loadFromFile(const std::string& name, const std::string& fileName);
	void setDefaultFont(const std::string& name);
	void setDefaultFont();
	const sf::Font& getFont(const std::string& name);
	const sf::Font& getDefaultFont(); 
};