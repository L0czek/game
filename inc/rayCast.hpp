#pragma once

#include "Box2D/Box2D.h"
#include "scale.hpp"
#include <functional>
#include <map>
#include <optional>
#include "entity.hpp"

namespace RayCast{

struct Info{
	b2Fixture* mTarget;
	sf::Vector2i mPoint;
	sf::Vector2i mNormal;
	float mDistance;
};

template<typename T>
std::optional<Info> findNearst(b2World& world,const sf::Vector2<T>& start,const sf::Vector2<T>& end) {
	struct RayCastCallback :public b2RayCastCallback{
		std::optional<Info> mInfo;
		float ReportFixture(b2Fixture *fixture, const b2Vec2 &point, const b2Vec2 &normal, float32 fraction) override{
			float distance = Scale::Box2SFML(fraction);
			if(!mInfo || mInfo->mDistance > distance){
				mInfo = {fixture, sf::Vector2i{ static_cast<int>(Scale::Box2SFML(point.x)), static_cast<int>(Scale::Box2SFML(point.y)) }, sf::Vector2i{ static_cast<int>(Scale::Box2SFML(normal.x)), static_cast<int>(Scale::Box2SFML(normal.y)) }, distance};
			}
			return fraction;
		}
	}mRayCastCallback;
	world.RayCast(&mRayCastCallback, Scale::SFML2Box(start), Scale::SFML2Box(end));
	return mRayCastCallback.mInfo;
}

template<typename T>
bool isInRange(b2World& world,const Entity& entity,const sf::Vector2<T>& start,const sf::Vector2<T>& end) {
	struct RayCastCallback :public b2RayCastCallback{
		bool mIsInRange = false;
		const Entity& entity;

		float ReportFixture(b2Fixture *fixture, const b2Vec2 &point, const b2Vec2 &normal, float32 fraction) override{
			if(fixture->GetBody()->GetUserData() == reinterpret_cast<void*>(&entity)){
				mIsInRange = true;
				return 0;
			}
			return 1;
		}
	}mRayCastCallback;
	world.RayCast(&mRayCastCallback, Scale::SFML2Box(start), Scale::SFML2Box(end));
	return mRayCastCallback.mIsInRange;
}

template<typename T,typename Callback>
void forAllInRange(b2World& world,const sf::Vector2<T>& start,const sf::Vector2<T>& end,Callback callback) {
	struct RayCastCallback :public b2RayCastCallback{
		Callback mCallback;
		RayCastCallback(Callback mCallback) :mCallback(mCallback) {}
		float ReportFixture(b2Fixture *fixture, const b2Vec2 &point, const b2Vec2 &normal, float32 fraction) override{
			mCallback(*reinterpret_cast<ContactInterface*>(fixture->GetBody()->GetUserData()));
			return 1;
		}
	}mRayCastCallback{std::move(callback)};
	world.RayCast(&mRayCastCallback, Scale::SFML2Box(start), Scale::SFML2Box(end));
}

template<typename T,typename Callback>
void forAllInRangeInOrder(b2World& world,const sf::Vector2<T>& start,const sf::Vector2<T>& end,Callback callback) {
	struct RayCastCallback :public b2RayCastCallback{
		std::map<float32, std::function<void()>> mCallbacks;
		const Callback& mCallback;
		RayCastCallback(const Callback& mCallback) :mCallback(mCallback){}
		float ReportFixture(b2Fixture *fixture, const b2Vec2 &point, const b2Vec2 &normal, float32 fraction) override{
			mCallbacks.insert(std::make_pair(fraction, [this, fixture](){
				this->mCallback(*reinterpret_cast<ContactInterface*>(fixture->GetBody()->GetUserData()));
			}));
			return 1.0f;
		}
		void excecute() const{
			for(const auto &i : mCallbacks){
				i.second();
			}
		}
	}mRayCastCallback{callback};
	world.RayCast(&mRayCastCallback, Scale::SFML2Box(start), Scale::SFML2Box(end));
	mRayCastCallback.excecute();
}

} 