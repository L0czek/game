#pragma once

#include "Box2D/Box2D.h"

class Player;
class Platform;
class LaserFly;
class Lift;
class Sensor;
class EntityVisitor;

class ContactInterface{
public:
	virtual void BeginContact(ContactInterface& entity) =0;
	virtual void EndContact(ContactInterface& entity) =0;

	virtual void BeginContact(Player& player) {}
	virtual void BeginContact(Platform& platform) {}

	virtual void EndContact(Player& player) {}
	virtual void EndContact(Platform& platform) {}

	virtual void BeginContact(Sensor& player) {}
	virtual void EndContact(Sensor& platform) {}

	virtual void BeginContact(Lift& player) {}
	virtual void EndContact(Lift& platform) {}
	
	virtual void BeginContact(LaserFly& player) {}
	virtual void EndContact(LaserFly& platform) {}

	virtual void accept(EntityVisitor& visitor) =0;

};

class ContactListener :public b2ContactListener{
public:
	void BeginContact(b2Contact* contact) override;
	void EndContact(b2Contact* contact) override;
};
