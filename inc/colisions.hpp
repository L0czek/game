#pragma once

#include <cstdint>

enum class ObjectTypes{
	Player =     0x0001,
	Enemy =      0x0002,
	Platform =   0x0004,

};

constexpr uint16_t collideWithAll() {
	return 0xffff;
}

template<typename T>
uint16_t collideWith(T t) {
	return t;
}

template<typename T,typename... Types>
uint16_t collideWith(T t,Types&&... types) {
	return t | collideWith(std::forward<Types>(types)...);
}



template<typename... Types>
void setCollisionFilter(b2Fixture& fixture,ObjectTypes self,Types&&... types) {
	b2Filter = fixture.GetFilterData();
	filter.categoryBits = static_cast<uint16_t>(self);
	filter.maskBits = collideWith(types...);
	filter.groupIndex = 0;
	fixture.SetFilterData(filter);
}