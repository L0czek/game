#pragma once

#include <SFML/Graphics.hpp>

#include "contactListener.hpp"
#include "entityVisitor.hpp"
#include "networkInterfaces.hpp"

class Player;
class Platform;

class Entity :public sf::Drawable {

public:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const=0;
	virtual void load() =0;
	virtual void update() =0;
};

class LivingEntity :public Entity{
	float mHealth;
public:
	virtual void takeDamage(float damage) { mHealth -= damage; }
	virtual float getHealth() const{ return mHealth; }
	virtual float totalHealth() const{ return 0; }
	virtual void setHealth(float health) { mHealth = health; }
	virtual bool isDead() const{ return mHealth <= 0; }
	virtual void applyImpulse(const b2Vec2& vector) {}
	virtual void hitFrom(const sf::Vector2f& position) {}
};

class DynamicEntity :public Entity, public DynamicEntityNetworkInterface{};
class Enemy :public LivingEntity, public EnemyNetworkInterface{};