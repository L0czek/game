#pragma once

#include "packet.hpp"

class DynamicEntityNetworkInterface{
	std::size_t mNetworkId;
public:
	void setNetworkId(std::size_t id) { mNetworkId = id; }
	std::size_t getNetworkId() const{ return mNetworkId; }
	virtual DynamicEntitySyncPacket getState() const=0;
	virtual void setState(const DynamicEntitySyncPacket& packet) =0;
};

class EnemyNetworkInterface{
	std::size_t mNetworkId;
public:
	void setNetworkId(std::size_t id) { mNetworkId = id; }
	std::size_t getNetworkId() const{ return mNetworkId; }
	virtual EnemySyncPacket getState() const=0;
	virtual void setState(const EnemySyncPacket& packet) =0;
};

class PlayerNetworkInterface{
	std::string mNetworkId;
public:
	void setNetworkId(const std::string& id) { mNetworkId = id; }
	const std::string& getNetworkId() const{ return mNetworkId; }
	virtual PlayerSyncPacket getState() const=0;
	virtual void setState(const PlayerSyncPacket& packet) =0;
};
