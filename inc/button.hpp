#pragma once

#include <SFML/Graphics.hpp>
#include <functional>

class Button :public sf::Drawable{
	sf::RectangleShape mShape;
	sf::Text mText;
	std::function<void()> mOnClick;
	bool mClicked = false;
public:
	Button();
	template<typename Callback>
	void onClick(Callback&& callback) {
		mOnClick = callback;
	}
	void setBounds(const sf::Vector2i& position,const sf::Vector2i& size);
	void setColor(const sf::Color& color);
	void setText(const std::string& text);
	void setFont(const sf::Font& font,unsigned int fontSize,const sf::Color& color = sf::Color::Black);
	void update(const sf::Vector2i& mousePos);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};