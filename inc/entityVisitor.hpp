#pragma once

class Player;
class Platform;
class LaserFly;
class Lift;

class EntityVisitor{
public:
	virtual void visit(Player& entity) =0;
	virtual void visit(Platform& entity) =0;
	virtual void visit(LaserFly& entity) =0;
	virtual void visit(Lift& entity) =0;
};

class TypeCheckVisitor :public EntityVisitor{
protected:
	bool mValue;
	void correct() { mValue = true; }
	void incorrect() { mValue = false; }
public:
	virtual void visit(Player& entity) { incorrect(); }
	virtual void visit(Platform& entity) { incorrect(); }
	virtual void visit(LaserFly& entity) { incorrect(); }
	virtual void visit(Lift& entity) { incorrect(); }
	bool getResult() const{ return mValue; }
};