#pragma once

#include <SFML/Graphics.hpp>

class TextInput :public sf::Drawable{
	sf::RectangleShape mShape;
	sf::Text mText;
	std::string mString;
	std::size_t mMaxSize = 20;
public:
	TextInput();
	std::string getString();
	void addLetter(char letter);
	void registerCallback();
	void setColor(const sf::Color& color);
	void setBounds(const sf::Vector2i& position,const sf::Vector2i& size);
	void setFont(const sf::Font& font,unsigned int fontSize,const sf::Color& color = sf::Color::Black);
	void update();
	void setMaxSize(std::size_t size);
	void backspace();
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};