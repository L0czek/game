#pragma once

#include <SFML/Graphics.hpp>

class HUD :public sf::Drawable{
	sf::Font mFont;
	sf::Text mHealth;
	sf::Text mAmmo;
public:
	HUD(const std::string& fontName);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};