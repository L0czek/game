#pragma once

#include "contactListener.hpp"
#include "common.hpp"
#include "Box2D/Box2D.h"
#include "scale.hpp"
#include <functional>

class GroundSensor :public ContactInterface{
public:
	b2Body& mBody;
	UniqueFixture mFixture;
	std::size_t mGroundContact;
public:
	GroundSensor(b2Body& body,sf::Vector2i position,sf::Vector2i size) :mBody(body){
		b2PolygonShape shape;
		shape.SetAsBox(Scale::SFML2Box(size.x), Scale::SFML2Box(size.y), b2Vec2{Scale::SFML2Box(position.x), Scale::SFML2Box(position.y)}, 0);
		b2FixtureDef fixture;
		fixture.shape = &shape;
		fixture.isSensor = true;
		mFixture = UniqueFixture(mBody.CreateFixture(&fixture), [this](b2Fixture* ptr){ mBody.DestroyFixture(ptr); });
		mFixture->SetUserData(this);
		mGroundContact = 0;
	}
	void BeginContact(ContactInterface& a) override{ a.BeginContact(*this); }
	void EndContact(ContactInterface& b)   override{ b.EndContact(*this); }

	void BeginContact(const Platform&) override{ mGroundContact++; }
	void EndContact(const Platform&)   override{ mGroundContact--; }
	void BeginContact(const Lift&) override{ mGroundContact++; }
	void EndContact(const Lift&)   override{ mGroundContact--; }
	void BeginContact(const LaserFly&) override{ mGroundContact++; }
	void EndContact(const LaserFly&)   override{ mGroundContact--; }

	bool isOnGround() const{ return mGroundContact>0; }
	void clearFlag() { mGroundContact=0; }
	void accept(EntityVisitor& visitor) override{}
};