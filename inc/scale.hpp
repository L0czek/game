#pragma once

#include <SFML/Graphics.hpp>
#include "Box2D/Box2D.h"

struct GlobalConsts{
	static float Scale;
};

namespace Scale{
	template<typename T>
	T Box2SFML(T t) {
		return t * GlobalConsts::Scale;
	}
	template<typename T>
	float SFML2Box(T t) {
		return t / GlobalConsts::Scale;
	}
	template<typename T>
	b2Vec2 SFML2Box(const sf::Vector2<T>& vec) {
		return b2Vec2{ SFML2Box(vec.x), SFML2Box(vec.y) };
	}
	template<typename T>
	sf::Vector2<T> Box2SFML(const b2Vec2& vec) {
		return sf::Vector2<T>{Box2SFML(vec.x), Box2SFML(vec.y)};
	}
}