#pragma once

#include "platform.hpp"
#include "sprite2.hpp"
#include <memory>
#include "map.hpp"

class Lift :public ContactInterface, public DynamicEntity{
	std::unique_ptr<SingleStringSprite> mSprite;
	sf::Vector2f mStartPoint;
	float mHeight;
	float mSpeed;
public:
	Lift(b2World& world,const sf::Vector2u& position,const sf::Vector2u& size,float mHeight,float speed,const std::shared_ptr<sf::Texture>& texture,float friction = 1);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void update() override;

	void BeginContact(ContactInterface& entity) override { entity.BeginContact(*this); }
	void EndContact(ContactInterface& entity) override { entity.EndContact(*this); }
	void accept(EntityVisitor& visitor) override{ visitor.visit(*this); }
	void load() override{}

	DynamicEntitySyncPacket getState() const override;
	void setState(const DynamicEntitySyncPacket& packet)  override;
};

class LiftFactory :public Map::EntityFactory{
	std::size_t mProduced = 0;
	float mHeight;
	float mSpeed;
	char mId;
	std::shared_ptr<sf::Texture> texture;
public:
	LiftFactory(float mHeight,float mSpeed,char id,std::shared_ptr<sf::Texture> texture) :	mHeight(mHeight), mSpeed(mSpeed), mId(id), texture(texture){}
	char id() const override{ return mId; }
	constexpr bool canConsolidate() const override{ return true; }
	void make(Map& map,const sf::Vector2u& position,const sf::Vector2u& size) override{
        auto it = map.addDynamicEntity<Lift>(position + sf::Vector2u{0, 40}, sf::Vector2u{size.x, 10}, mHeight,mSpeed, texture);
        (*it)->setNetworkId(mProduced++);
    }
};