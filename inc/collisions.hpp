#pragma once

#include <cstdint>

enum class ObjectType{
	Player =     0x0001,
	Enemy =      0x0002,
	Platform =   0x0004,
	Sensor = 	 0x0008
};

constexpr uint16_t collideWithAll() {
	return 0xffff;
}

constexpr uint16_t collideWithNone() {
	return 0x0000;
}

template<typename T>
constexpr uint16_t collideWith(T t) {
	return static_cast<uint16_t>(t);
}

template<typename T,typename... Types>
constexpr uint16_t collideWith(T t,Types&&... types) {
	return t | collideWith(std::forward<Types>(types)...);
}

template<typename... Types>
constexpr uint16_t collideWithAllExcept(Types&&... types) {
	return collideWithAll() & ~collideWith(types...);
}
template<typename... Types>
void setCollisionFilter(b2Fixture& fixture,ObjectType self,Types&&... types) {
	b2Filter filter = fixture.GetFilterData();
	filter.categoryBits = static_cast<uint16_t>(self);
	filter.maskBits = collideWith(types...);
	filter.groupIndex = 0;
	fixture.SetFilterData(filter);
}