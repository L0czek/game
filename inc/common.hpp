#pragma once

#include <type_traits>
#include <tuple>
#include <sstream>
#include <string>
#include <memory>
#include <unordered_map>
#include "Box2D/Box2D.h"
#include <functional>
#include <ctime>
#include <iomanip>
#include <iostream>

typedef std::unique_ptr<b2Body,std::function<void(b2Body*)>> UniqueBody;
typedef std::unique_ptr<b2Fixture,std::function<void(b2Fixture*)>> UniqueFixture;

template<typename T1,typename T2>
auto operator/(const sf::Vector2<T1>& a,const sf::Vector2<T2>& b) {
	if constexpr(std::is_floating_point<T1>::value || std::is_floating_point<T2>::value){
		return sf::Vector2f{ static_cast<float>(a.x) / static_cast<float>(b.x),
							 static_cast<float>(a.y) / static_cast<float>(b.y) };
	}else if constexpr(std::is_signed<T1>::value || std::is_signed<T2>::value){
		return sf::Vector2i{ static_cast<int>(a.x) / static_cast<int>(b.x),
							 static_cast<int>(a.y) / static_cast<int>(b.y) };
	}else{
		return sf::Vector2u{ static_cast<unsigned int>(a.x) / static_cast<unsigned int>(b.x),
							 static_cast<unsigned int>(a.y) / static_cast<unsigned int>(b.y) };
	}
}
template<typename T1,typename T2>
auto operator*(const sf::Vector2<T1>& a,const sf::Vector2<T2>& b) {
	if constexpr(std::is_floating_point<T1>::value || std::is_floating_point<T2>::value){
		return sf::Vector2f{ static_cast<float>(a.x) * static_cast<float>(b.x),
							 static_cast<float>(a.y) * static_cast<float>(b.y) };
	}else if constexpr(std::is_signed<T1>::value || std::is_signed<T2>::value){
		return sf::Vector2i{ static_cast<int>(a.x) * static_cast<int>(b.x),
							 static_cast<int>(a.y) * static_cast<int>(b.y) };
	}else{
		return sf::Vector2u{ static_cast<unsigned int>(a.x) * static_cast<unsigned int>(b.x),
							 static_cast<unsigned int>(a.y) * static_cast<unsigned int>(b.y) };
	}
}
template<typename T1,typename T2>
sf::Vector2<T1> operator/(const sf::Vector2<T1>& v,T2 b) {
	return sf::Vector2<T1>{ v.x / b, v.y / b};
}
template<typename T1,typename T2>
sf::Vector2<T1> operator*(const sf::Vector2<T1>& v,T2 b) {
	return sf::Vector2<T1>{ v.x * b, v.y * b};
}
template<typename T1,typename T2>
typename std::enable_if<!std::is_same<T2,float>::value,sf::Vector2<T1>>::value operator*(const sf::Vector2<T1>& a,T2 b) {
	return sf::Vector2<T1>{ a.x * b, a.y * b};
}
template<typename T>
bool operator<(const sf::Vector2<T>& a,const sf::Vector2<T>& b){
	return a.x < b.x || a.y < b.y;
}
template<typename T,typename V>
sf::Vector2<T> convert(const V& v) {
	return sf::Vector2<T>{static_cast<T>(v.x), static_cast<T>(v.y)};
}
template<typename T,typename X,typename Y>
sf::Vector2<T> convert(X x,Y y) {
	return sf::Vector2<T>{static_cast<T>(x), static_cast<T>(y)};
}

template<typename... Strings>
class StringBuilder{
	std::stringstream mSs;
	std::tuple<Strings...> mStrings;
	template<std::size_t i> void Iterate() {
		mSs << std::get<i>(mStrings);
		if constexpr(i < sizeof...(Strings)-1){
			Iterate<i+1>();
		}
	} 
public:
	StringBuilder(Strings&&... strings) :mStrings(std::forward<Strings>(strings)...) {}
	std::string operator()() && { Iterate<0>(); return mSs.str(); }
};

template<typename... Strings>
std::string Concat(Strings&&... strings) {
	return StringBuilder<Strings...>{ std::forward<Strings>(strings)... }();
}

template<typename... Strings>
std::ostream& log(Strings&&... strings) {
	auto time = std::time(nullptr);
	auto tm = *std::localtime(&time);
	std::cout << "[ " << std::put_time(&tm, "%c %Z") << " ]: " << Concat(std::forward<Strings>(strings)...) << "\n";
	return std::cout;
}

template<typename RequestedType,typename EnabledType,typename... ParameterPack>
using enable_if_in_t = typename std::enable_if<std::disjunction<std::is_same<ParameterPack, RequestedType>...>::value, EnabledType>::type;

template<typename RequestedType,typename... ParameterPack>
using is_in_pack = std::disjunction<std::is_same<ParameterPack, RequestedType>...>;

const sf::Vector2i one2i = sf::Vector2i{1, 1};