#pragma once

#include "game.hpp"
#include "button.hpp"
#include "fontContainer.hpp"
#include "progressBar.hpp"
#include "textInput.hpp"
#include <vector>
#include <memory>

class InitialScreen :public Game::Screen{
	sf::Sprite mBackground;
	TextInput mNick;
	Button mMultiHost;
	Button mMultiClient;
	Button mStartButton;
	Button mExitButton;
	std::shared_ptr<sf::Music> mIntroMusic;
	void startCallback();
	void exitCallback();
	void multiHostCallback();
	void multiClientCallback();
public:
	InitialScreen(Game& mGame,sf::RenderWindow& mWindow);
	void create() override;
	void update() override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

class LoadingScreen :public Game::LoadingScreen{
	sf::Text mLoadingText;
	sf::Sprite mBackground;
	ProgressBar mProgressBar;
	std::shared_ptr<sf::Music> mLoadingMusic;
public:
	LoadingScreen(Game& mGame,sf::RenderWindow& mWindow);
	void create() override;
	void update() override;
	void updateProgress(float progress) override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

class GameOverScreen :public Game::Screen{
	sf::Text mGameOverText;
	Button mRestart;
	Button mExit;
	std::shared_ptr<sf::Music> mFailMusic;
	bool mDrawRestart;
	std::atomic_bool mRestartReq = false;
	void restartCallback();
	void exitCallback();
public:
	GameOverScreen(Game& mGame,sf::RenderWindow& mWindow);
	void create() override;
	void update() override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

class GameWinScreen :public Game::Screen{
	sf::Text mGameWinText;
	Button mNextLevel;
	Button mExit;
	bool mDrawNextLevel;
	std::atomic_bool mNextLevelRequest = false;
	std::shared_ptr<sf::Music> mSuccessMusic;
	void exitCallback();
	void nextLevelCallback();
public:
	GameWinScreen(Game& mGame,sf::RenderWindow& mWindow);
	void create() override;
	void update() override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

class GameMultiHostScreen :public Game::Screen{
	sf::Sprite mBackground;
	std::vector<sf::Text> mNicks;
	Button mStartGame;
	Button mBack;
	void startCallback();
	void backCallback();
public:
	GameMultiHostScreen(Game& mGame,sf::RenderWindow& mWindow);
	void create() override;
	void update() override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

class GameMultiClientScreen :public Game::Screen{
	sf::Sprite mBackground;
	TextInput mIp;
	Button mConnect;
	Button mBack;
	void connectCallback();
	void backCallback();
	void loadCallback();
public:
	GameMultiClientScreen(Game& mGame,sf::RenderWindow& mWindow);
	void create() override;
	void update() override;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};