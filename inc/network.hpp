#pragma once

#include <SFML/Network.hpp>
#include <set>
#include <map>
#include <thread>
#include <atomic>
#include <memory>
#include <functional>
#include <mutex>
#include "entity.hpp"
#include "packet.hpp"
#include "networkInterfaces.hpp"

class NetworkServer{
	std::string mName;
	sf::TcpListener mTcpListener;
	std::atomic_bool mAcceptConnections;
	std::atomic_bool mRunSyncLoop;
	std::atomic_bool mNewDataToSync;
	std::mutex mDataLock;
	std::unique_ptr<std::thread> mNetworkThread;
	std::unique_ptr<std::thread> mSyncThread;
	std::map<std::string, std::unique_ptr<sf::TcpSocket>> mClients;
	std::map<std::size_t, DynamicEntitySyncPacket> mDynamicEntities;
	std::map<std::size_t, EnemySyncPacket> mEnemies;
	std::map<std::string, PlayerSyncPacket> mPlayers;
	void acceptConnectionsLoop();
	void syncLoop();
public:
	NetworkServer(short unsigned int port,const std::string& name);
	~NetworkServer();
	std::map<std::string, std::unique_ptr<sf::TcpSocket>>& getClients() { return mClients; }
	void sync();
	void setEnemiesState(const std::set<std::unique_ptr<Enemy>>& set);
	void setDynamicEntitiesState(const std::set<std::unique_ptr<DynamicEntity>>& set);
	void setPlayerState(const std::unique_ptr<Player>& player);
	void getNetworkPlayersState(std::vector<std::unique_ptr<Player>>& players);
	void update();
	void send(const std::string& target,Packet& packet);
	void sendToAll(Packet& packet);
	void acceptConnections();
	void stopAccepting();
	void clearQueue();
	void waitForClientsToLoad();
	void startSync();
	void stopSync();
	const std::map<std::string, std::unique_ptr<sf::TcpSocket>>& getClients() const{ return mClients; }
};

class NetworkClient{
	std::string mName;
	sf::TcpSocket mSocket;
	std::function<void()> mOnLoad;
	std::function<void()> mOnReset;
	std::function<void()> mOnNextLevel;
	std::atomic_bool mRunSyncLoop;
	std::mutex mDataLock;
	std::unique_ptr<std::thread> mSyncThread;
	std::map<std::size_t, DynamicEntitySyncPacket> mDynamicEntities;
	std::map<std::size_t, EnemySyncPacket> mEnemies;
	std::map<std::string, PlayerSyncPacket> mPlayers;
	bool mConnected = false;
	void syncLoop();
public:
	~NetworkClient();
	StartGame waitForServerToLoad();
	PlayerList getPlayerList();
	sf::Vector2u getPlayerPosition();
	void setPlayerState(std::unique_ptr<Player>& player);
	void getNetworkPlayersState(std::vector<std::unique_ptr<Player>>& players);
	void getDynamicEntitiesState(std::set<std::unique_ptr<DynamicEntity>>& set);
	void getEnemiesState(const std::set<std::unique_ptr<Enemy>>& set);
	void send(Packet& packet);
	NetworkClient(const std::string& name);
	void connectTo(const std::string& ip,short unsigned int port);
	void update();
	template<typename Callback>
	void onLoad(Callback callback) {
		mOnLoad = std::move(callback);
	}
	template<typename Callback>
	void onReset(Callback callback) {
		mOnReset = callback;
	}
	template<typename Callback>
	void onNextLevel(Callback callback) {
		mOnNextLevel = callback;
	}
	void startSync();
	void stopSync();
};