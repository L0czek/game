#pragma once

#include <random>
#include <type_traits>

class RandomGenerator{
	std::mt19937 mGen;
	std::random_device rd;
	RandomGenerator() :mGen(rd()){
	}
public:
	static RandomGenerator& getInstance() {
		static RandomGenerator mGenerator;
		return mGenerator;
	}
	template<typename Type>
	Type getNumber(Type start, Type end) {
		if constexpr(std::is_floating_point<Type>::value){
			std::uniform_real_distribution<> dist(start, end);
			return dist(mGen);
		}else{
			 std::uniform_int_distribution<> dist(start, end);
			 return dist(mGen);
		}
	}
};