#pragma once

#include <SFML/Graphics.hpp>

class ProgressBar :public sf::Drawable{
	sf::RectangleShape mOutline;
	sf::RectangleShape mInside;
	sf::Vector2f mOffset;
	sf::Vector2f mSize;
public:
	ProgressBar(const sf::Vector2f& offset = {0,0},const sf::Vector2f& size = {0,0});
	void update(const sf::Vector2f& position,float duty);
	void update(float duty);
	void setPosition(const sf::Vector2f& position);
	void setSize(const sf::Vector2f& size);
	void setColor(const sf::Color& inside,const sf::Color& outline);
	void draw(sf::RenderTarget& target,sf::RenderStates states) const override;
};