#pragma once

#include <SFML/Graphics.hpp>
#include "sprite2.hpp"
#include "contactListener.hpp"
#include "entity.hpp"
#include "sensor.hpp"
#include <memory>
#include "map.hpp"
#include "progressBar.hpp"

class LaserFly :public ContactInterface, public Enemy{
public:
	enum class State{
		GoLeft,
		GoRight,
		TurnLeft,
		TurnRight,
		LaserAttackForward,
		Jump,
		Death,
		Dead
	};
private:
	b2World& mWorld;
	std::unique_ptr<SingleStringSprite> mSprite;
	std::unique_ptr<ContactSensor<Platform,Lift,Player>> mGroundSensor;
	std::unique_ptr<ContactSensor<Platform,Lift>> mLeftGroundSensor;
	std::unique_ptr<ContactSensor<Platform,Lift>> mRightGroundSensor;
	std::unique_ptr<ContactSensor<Platform,Lift>> mLeftRearSensor;
	std::unique_ptr<ContactSensor<Platform,Lift>> mRightRearSensor;
	State mState;
	sf::Sprite mLaserSprite;
	std::shared_ptr<sf::Texture> mLaserTexture;
	sf::Clock mLaserClock;
	float mDistanceToPlayer;
	sf::Vector2u mLaserSingleFrame;
	sf::Vector2u mLaserSpriteCordinates;
	ProgressBar mHealthBar{ sf::Vector2f{-75, -60}, sf::Vector2f{150, 5} };
	bool checkForPlayerInRange(const sf::Vector2i& direction);
	void attackForward();
	void jump(const sf::Vector2f& start,const sf::Vector2f& destination);
	void turnRight();
	void turnLeft();
	bool mNetworkDeath = false;
public:
	LaserFly(b2World& world,const sf::Vector2u& position);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void update() override;
	void load() override;
	void BeginContact(ContactInterface& entity) override { entity.BeginContact(*this); }
	void EndContact(ContactInterface& entity) override { entity.EndContact(*this); }
	void applyImpulse(const b2Vec2& vector) override { b2Body& body = *mSprite->getBody(); body.ApplyForce(vector, body.GetWorldCenter(), true); }
	void accept(EntityVisitor& visitor) override{ visitor.visit(*this); }
	void hitFrom(const sf::Vector2f& position) override;
	constexpr float totalHealth() const override{ return 100; }
	EnemySyncPacket getState() const override;
	void setState(const EnemySyncPacket& packet) override;
};

struct LaserFlyFactory :public Map::EntityFactory{
	std::size_t mProduced = 0;
    constexpr char id() const override{ return 'F'; }
    constexpr bool canConsolidate() const override{ return false; }
    void make(Map& map,const sf::Vector2u& position,const sf::Vector2u& size) override{
        auto it = map.addEnemy<LaserFly>(position + sf::Vector2u{0, 40});
        (*it)->setNetworkId(mProduced++);
    }
};