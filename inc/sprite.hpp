/*

#pragma once

#include <SFML/Graphics.hpp>
#include "Box2D/Box2D.h"

#include <unordered_map>
#include <utility>
#include <functional>

#include "scale.hpp"

template<typename _SpriteSheetId = std::string,typename _AnimationsId = std::string>
class Sprite :public sf::Drawable {
	struct AnimationData{
		std::size_t mStartX = 0;
		std::size_t mStartY = 0;
		std::size_t mStopX = 0;
		std::size_t mStopY = 0;
		float mDeltaTime = 0.0;
		bool mRepeat = true;
		bool mIsRunning = false;
	};
	struct SpriteSheetData{
			sf::Image mImage;
		sf::Vector2u mSingleSize;
		std::size_t mMaxX;
		std::size_t mMaxY;
		std::map<_AnimationsId,AnimationData> mAnimations;
	};
public:
	typedef std::map<_SpriteSheetId,SpriteSheetData> SpriteSheetType; 
private:
	sf::Sprite mSprite;
	sf::Texture mTexture;
	std::unique_ptr<b2Body,std::function<void(b2Body*)>> mBody;
	SpriteSheetType mSpriteSheet;
	b2World& mWorld;

	typename SpriteSheetType::iterator mCurrentSprite;
	typename std::map<_AnimationsId,AnimationData>::iterator mCurrentAnimation;
	typename std::map<_AnimationsId,AnimationData>::iterator mAnimationEnd;
	
	std::size_t mChunkX; // Current Image info
	std::size_t mChunkY;
	std::size_t mMaxX;
	std::size_t mMaxY;
	
	sf::Clock mClock;	// Animation Clock
public:
	Sprite(b2World& world,const b2BodyDef& bodyDef);
	Sprite(const Sprite&) = delete;
	Sprite(Sprite&) = delete;
	Sprite(Sprite&&) = default;
	~Sprite() = default;

	void LoadFromFile(const std::string& fname,const _SpriteSheetId& Id,std::size_t nWidth = 1,std::size_t nHeight = 1);
	void NextChunk();
	void SetChunk(std::size_t x = 0,std::size_t y = 0);
	void SetImage(const _SpriteSheetId& Id);

	void SetPosition(float x,float y);
	void SetOrigin(float x,float y);
	void SetScale(float x,float y);
	void SetRotation(float angle);

	void SetPosition(const sf::Vector2f& v);
	void SetOrigin(const sf::Vector2f& v);
	void SetScale(const sf::Vector2f& v);

	void SetVelocity(const sf::Vector2f& v);
	void SetVelocity(float x,float y);

	void SetColor(const sf::Color& color);
	void SetTextureRect(const sf::IntRect &rectangle);

	void SetAnimation(const _AnimationsId& Id,std::size_t startX,std::size_t startY,std::size_t stopX,std::size_t stopY,float deltaTime,bool repeat = true);
	void SetAnimation(const _AnimationsId& Id,float deltaTime,bool repeat = true);
	void RunAnimation(bool repeat = false);
	bool AnimationIsRunning() const;
	void SelectAnimation(const _AnimationsId& Id);
	void StopAnimation();
	void AnimationStep();

	void CreateFixture(const b2FixtureDef& fixtureDef);

	sf::Vector2f GetPosition() const;
	sf::Texture& GetTexture();
	sf::Sprite& GetSprite();
	const std::unique_ptr<b2Body,std::function<void(b2Body*)>>& GetBody() const;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void Update();
};

template<typename _SpriteSheetId,typename _AnimationsId> 
Sprite<_SpriteSheetId,_AnimationsId>::Sprite(b2World& world,const b2BodyDef& bodyDef) :mWorld(world) {
	mSprite.setTexture(mTexture);
	mBody = std::unique_ptr<b2Body,std::function<void(b2Body*)>>(mWorld.CreateBody(&bodyDef),[this](b2Body * ptr){ this->mWorld.DestroyBody(ptr); });
	mChunkY = 0;
	mChunkX = 0;
	mMaxX = 0;
	mMaxY = 0;
}

template<typename _SpriteSheetId,typename _AnimationsId> void Sprite<_SpriteSheetId,_AnimationsId>::LoadFromFile(const std::string& fname,const _SpriteSheetId& Id,std::size_t nWidth,std::size_t nHeight) {
	sf::Image image;
	if(!image.loadFromFile(fname)){
		throw std::runtime_error("no such file");
	}	
	sf::Vector2u size = image.getSize();
	size.x /= nWidth;	
	size.y /= nHeight;
	mSpriteSheet.insert(std::make_pair(Id, SpriteSheetData{ std::move(image), size, nWidth, nHeight} ));
}

template<typename _SpriteSheetId,typename _AnimationsId> void Sprite<_SpriteSheetId,_AnimationsId>::SetChunk(std::size_t x,std::size_t y) {
	if(x < mMaxX && y < mMaxY && mCurrentSprite != mSpriteSheet.end()){
		sf::Vector2u pos = mCurrentSprite->second.mSingleSize;
		const auto& size = mCurrentSprite->second.mSingleSize;
		pos.x *= x;
		pos.y *= y;
		mChunkX = x;
		mChunkY = y;
		mSprite.setTextureRect(sf::IntRect{ static_cast<int>(pos.x), static_cast<int>(pos.y), static_cast<int>(size.x), static_cast<int>(size.y)});
	}
}

template<typename _SpriteSheetId,typename _AnimationsId> void Sprite<_SpriteSheetId,_AnimationsId>::NextChunk() {
	std::size_t x = mChunkX + 1;
	std::size_t y = mChunkY;
	if(x == mMaxX){
		y++;
		x = 0;
		if(y == mMaxY){
			y = 0;
		}
	}
	SetChunk(x,y);
}

template<typename _SpriteSheetId,typename _AnimationsId> void Sprite<_SpriteSheetId,_AnimationsId>::SetImage(const _SpriteSheetId& Id) {
	mCurrentSprite = mSpriteSheet.find(Id);
	if(mCurrentSprite != mSpriteSheet.end()){
		auto iSize = mCurrentSprite->second.mImage.getSize();
		auto tSize = mTexture.getSize();
		const auto& pSize = mCurrentSprite->second.mSingleSize;
		if(tSize.x < iSize.x || tSize.y < iSize.y){
			mTexture.create(iSize.x,iSize.y);
			mSprite.setTextureRect(sf::IntRect{ 0, 0, static_cast<int>(pSize.x), static_cast<int>(pSize.y)});
		}
		mTexture.update(mCurrentSprite->second.mImage);
		mMaxX = mCurrentSprite->second.mMaxX;
		mMaxY = mCurrentSprite->second.mMaxY;
	}
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetPosition(float x,float y){
	mSprite.setPosition(x,y);
	mBody->SetTransform(b2Vec2{x,y},mBody->GetAngle());
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetOrigin(float x,float y) {
	mSprite.setOrigin(x,y);
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetScale(float x,float y) {
	mSprite.setScale(x,y);
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetRotation(float angle) {
	mSprite.setRotation(angle);
	mBody->SetTransform(mBody->GetPosition(),angle);
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetPosition(const sf::Vector2f& v) {
	mSprite.setPosition(v);
	mBody->SetTransform(b2Vec2{ Scale::SFML2Box( v.x ), Scale::SFML2Box( v.y ) },mBody->GetAngle());
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetOrigin(const sf::Vector2f& v) {
	mSprite.setOrigin(v);
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetScale(const sf::Vector2f& v) {
	mSprite.setScale(v);
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetVelocity(const sf::Vector2f& v) {
	mBody->SetLinearVelocity(b2Vec2{v.x,v.y});
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetVelocity(float x,float y) {
	mBody->SetLinearVelocity(b2Vec2{x,y});
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetColor(const sf::Color& color) {
	mSprite.setColor(color);
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetTextureRect(const sf::IntRect& rectangle) {
	mSprite.setTextureRect(rectangle);
}

template<typename _SpriteSheetId,typename _AnimationsId> sf::Vector2f Sprite<_SpriteSheetId,_AnimationsId>::GetPosition() const{
	auto position = mBody->GetPosition();
	return sf::Vector2f{ Scale::Box2SFML( position.x ), Scale::Box2SFML( position.y ) };
}

template<typename _SpriteSheetId,typename _AnimationsId> 
sf::Texture& Sprite<_SpriteSheetId,_AnimationsId>::GetTexture() {
	return mTexture;
}

template<typename _SpriteSheetId,typename _AnimationsId> 
sf::Sprite& Sprite<_SpriteSheetId,_AnimationsId>::GetSprite() {
	return mSprite;
}

template<typename _SpriteSheetId,typename _AnimationsId> 
const std::unique_ptr<b2Body,std::function<void(b2Body*)>>& Sprite<_SpriteSheetId,_AnimationsId>::GetBody() const{
	return mBody;
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mSprite,states);
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::Update() {
	mSprite.setPosition(GetPosition());
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetAnimation(const _AnimationsId& Id,std::size_t startX,std::size_t startY,std::size_t stopX,std::size_t stopY,float deltaTime,bool repeat){
	if(startX > mMaxX || startY > mMaxY || stopX < startX || stopY < startY || mCurrentSprite == mSpriteSheet.end()){
		throw std::runtime_error("Animation no avaible");
	}
	mCurrentSprite->second.mAnimations.insert(std::make_pair(Id,AnimationData{ startX, startY, stopX, stopY, deltaTime, repeat, true}));
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SetAnimation(const _AnimationsId& Id,float deltaTime,bool repeat){
	if(mMaxX == 0 || mMaxY == 0 || mCurrentSprite == mSpriteSheet.end()){
		throw std::runtime_error("Animation not avaible");
	}
	SetAnimation(Id,0,0,mMaxX-1,mMaxY-1,deltaTime,repeat);
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::AnimationStep(){
	if( mCurrentAnimation != mAnimationEnd && mCurrentAnimation->second.mIsRunning && mClock.getElapsedTime().asSeconds() >= mCurrentAnimation->second.mDeltaTime){
		mClock.restart();
		if(mChunkX == mCurrentAnimation->second.mStopX && mChunkY == mCurrentAnimation->second.mStopY){
			if(!mCurrentAnimation->second.mRepeat){
				StopAnimation();
			}
		}
		NextChunk();
	}
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::StopAnimation() {
	if(mCurrentAnimation != mAnimationEnd)
		mCurrentAnimation->second.mIsRunning = false;
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::SelectAnimation(const _AnimationsId& Id) {
	if(mCurrentSprite != mSpriteSheet.end()){
		mCurrentAnimation = mCurrentSprite->second.mAnimations.find(Id);
		if(mCurrentAnimation != mAnimationEnd){
			SetChunk(mCurrentAnimation->second.mStartX,mCurrentAnimation->second.mStartY);
		}
	}
}

template<typename _SpriteSheetId,typename _AnimationsId> 
bool Sprite<_SpriteSheetId,_AnimationsId>::AnimationIsRunning() const {
	return mCurrentAnimation != mAnimationEnd && mCurrentAnimation->second.mIsRunning;
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::RunAnimation(bool repeat) {
	if(mCurrentAnimation != mAnimationEnd){
		mCurrentAnimation->second.mIsRunning = true;
	}
}

template<typename _SpriteSheetId,typename _AnimationsId> 
void Sprite<_SpriteSheetId,_AnimationsId>::CreateFixture(const b2FixtureDef& fixtureDef) {
	mBody->CreateFixture(&fixtureDef);
}
*/