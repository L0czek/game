#pragma once

#include "Box2D/Box2D.h"
#include "common.hpp"
#include <list>
#include <vector>

class SingleFixture{
	UniqueFixture mFixture;
public:
	void setFixture(const UniqueBody& body,const b2FixtureDef& fixture) {
		mFixture = UniqueFixture(body->CreateFixture(&fixture), std::bind(&b2Body::DestroyFixture, body.get(), std::placeholders::_1));
	}
	b2Fixture& getFixture() {
		if(mFixture){
			return *mFixture;
		}else{
			throw std::runtime_error("no fixture");
		}
	}
	void removeFixture() {
		mFixture.reset();
	}
};

class MultiFixture{
	std::vector<UniqueFixture> mFixtures;
public:
	typedef std::vector<UniqueFixture>::iterator iterator;
	iterator addFixture(const UniqueBody& body,const b2FixtureDef& fixture) {
		return mFixtures.insert(mFixtures.end(), UniqueFixture(body->CreateFixture(&fixture), std::bind(&b2Body::DestroyFixture, body.get(), std::placeholders::_1)));
	}
	void removeFixture(iterator it) {
		mFixtures.erase(it);
	}
	void cleanFixtures() {
		mFixtures.clear();
	}
	b2Fixture& front() { return *mFixtures.front(); }
	b2Fixture& back() { return *mFixtures.back(); }
	iterator begin() { return mFixtures.begin(); }
	iterator end() { return mFixtures.end(); }
};