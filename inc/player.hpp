#pragma once

#include "entity.hpp"
#include "scale.hpp"
#include "sprite2.hpp"
#include "Box2D/Box2D.h"
#include "platform.hpp"
#include "sensor.hpp"
#include <memory>
#include <list>
#include <map>
#include <array>
#include "playerWeapon.hpp"
#include "progressBar.hpp"
#include "map.hpp"

class Player :public ContactInterface, public LivingEntity, public PlayerNetworkInterface {
public:
	enum class State{
		Walk,
		Jump,
		CrouchStart,
		CrouchIdle,
		CrouchEnd,
		AttackIdle,
		AttackJump,
		AttackWalk,
		SlideStart,
		SlideIdle,
		SlideShoot,
		SlideEnd,
		ReloadIdle,
		ReloadCrouch,
		Death,
		Dead
	};
	enum class Weapon{
		AK47,
		BaseBall
	};
private:
	AK47 mAK47;
	BaseBallStick mBaseBall;
	Weapon mEquippedWeapon = Weapon::AK47;
	State mState;
	static std::array<std::map<std::string,std::string>, 2> mSpriteTranslation;
	std::unique_ptr<MultiStringSprite> mSprite;
	std::unique_ptr<ContactSensor<Platform,Lift,LaserFly>> mGroundSensor;
	float mWalkSpeed = 1;
	float mRunSpeed = 10;
	sf::Clock mClock;
	ProgressBar mHealthBar{ sf::Vector2f{-50, -60}, sf::Vector2f{100, 5} };
	sf::Text mNick;
	bool mNetworkMode = false;
	mutable std::list<std::pair<sf::Vector2f, sf::Vector2f>> mAttackRequests;
	std::string resolveSpriteName(const std::string& name) const;
public:
	Player(const Player&) = delete;
	Player(Player&&) = default;
	Player(b2World& world,const sf::Vector2u& StartPosition);
	sf::Vector2f getPosition() const{ return mSprite->getPosition(); }
	void setPosition(const sf::Vector2f& position) { mSprite->setPosition(position.x, position.y); }
	void attack(const sf::Vector2f& position, const sf::Vector2f& direction);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void update() override;
	void load() override;
	void accept(EntityVisitor& visitor) override{ visitor.visit(*this); }
	constexpr float totalHealth() const override{ return 100; }
	void setNick(const std::string& nick) {
		setNetworkId(nick);
		mNick.setString(sf::String(nick));
	}
	std::string getNick() const{ return getNetworkId(); }
	void setNetworkMode(bool mode) { mNetworkMode = mode; }
	PlayerSyncPacket getState() const override;
	void setState(const PlayerSyncPacket& packet) override;
	void quickUpdate();
private:
	bool HavePassed(float seconds);
	void BeginContact(ContactInterface& entity) override { entity.BeginContact(*this); }
	void EndContact(ContactInterface& entity) override { entity.EndContact(*this); }
};

struct PlayerSpawns :public Map::EntityFactory{
    constexpr char id() const override{ return 'P'; }
    constexpr bool canConsolidate() const override{ return false; }
    void make(Map& map,const sf::Vector2u& position,const sf::Vector2u& size) override{;
        map.addSpawnPoint(position);
    }
};