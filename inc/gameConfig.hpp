#pragma once

struct GameConfig{
	sf::Vector2u mWindowSize;
	std::size_t mFrameLimit;
	sf::FloatRect mInitialViewport;
	short unsigned int mPort;
	b2Vec2 mGravity;
};
