#pragma once

#include <unordered_map>
#include <memory>
#include <SFML/Graphics.hpp>
#include <initializer_list>

struct Animation{
	float mDeltaTime;
	bool mRepeat = false;
	bool mIsRunning = false;
	bool mReversed = false;
	sf::Vector2u mPositionInCordinates;
};
struct Image{
	sf::Image mImage;
	sf::Vector2u mSingleSize;
	sf::Vector2u mPosition;
	sf::Vector2u mTotalSize;
	sf::Vector2u mMaxCordinates;
	//Animation mAnimation;
	Image(sf::Image mImage,sf::Vector2u mSingleSize,sf::Vector2u mPosition,sf::Vector2u mTotalSize,std::size_t x,std::size_t y) :mImage(std::move(mImage)), mSingleSize(mSingleSize), mPosition(mPosition), mTotalSize(mTotalSize), mMaxCordinates(x-1, y-1) {}
};

template<typename SpriteSheetId>
class ImageContainer{
	typedef std::unordered_map<SpriteSheetId, Image> SpriteSheetData;
	typedef std::map<std::string, std::shared_ptr<SpriteSheetData>> SpriteSheetArray;
	typedef std::map<std::string, std::pair<std::shared_ptr<sf::Texture>, sf::Image>> SingleImageArray;
	SpriteSheetArray mSpriteSheetArray;
	SingleImageArray mSingleImageArray;
	ImageContainer() {}
public:
	static ImageContainer& getInstance() { static ImageContainer mImageContainer; return mImageContainer; }
	void loadSingleImage(const std::string& imageName,const std::string& fileName);
	void loadSprite(const std::string& spriteSheet,const std::string& fileName,const SpriteSheetId& id,std::size_t nInWidth,std::size_t nInHeight);
	void loadSprite(const std::string& spriteSheet,std::initializer_list<std::tuple<std::string,SpriteSheetId,std::size_t,std::size_t>> list);
	std::shared_ptr<sf::Texture> getSingleTexture(const std::string& name) const;
	auto getSpriteSheets(const std::string& name) const;
	void clear();
};

template<typename SpriteSheetId>
void ImageContainer<SpriteSheetId>::loadSingleImage(const std::string& imageName,const std::string& fileName) {
	sf::Image img;
	if(!img.loadFromFile(fileName)){
		throw std::runtime_error(Concat("No such file: ",fileName));
	}
	std::shared_ptr<sf::Texture> texture = std::make_shared<sf::Texture>();
	auto size = img.getSize();
	texture->create(size.x, size.y);
	texture->update(img);
	mSingleImageArray.insert(std::make_pair(imageName, std::make_pair(texture, std::move(img))));
}
template<typename SpriteSheetId>
void ImageContainer<SpriteSheetId>::loadSprite(const std::string& spriteSheet,const std::string& fileName,const SpriteSheetId& id,std::size_t nInWidth,std::size_t nInHeight) {
	if(!mSpriteSheetArray[spriteSheet]){
		mSpriteSheetArray[spriteSheet] = std::make_shared<SpriteSheetData>();
	}
	sf::Image image;
	if(!image.loadFromFile(fileName)){
		throw std::runtime_error("Can not load image.");
	}
	sf::Vector2u size = image.getSize();
	sf::Vector2u singleSize = size;
	singleSize.x /= nInWidth;
	singleSize.y /= nInHeight;
	mSpriteSheetArray[spriteSheet]->insert(std::make_pair(id, Image{ std::move(image), singleSize, sf::Vector2u(0,0), size, nInWidth, nInHeight }));
}
template<typename SpriteSheetId>
void ImageContainer<SpriteSheetId>::loadSprite(const std::string& spriteSheet,std::initializer_list<std::tuple<std::string,SpriteSheetId,std::size_t,std::size_t>> list) {
	mSpriteSheetArray[spriteSheet] = std::make_shared<SpriteSheetData>();
	for(const auto &i : list){
		auto [ fileName, id, nInWidth, nInHeight] = i;
		loadSprite(spriteSheet, fileName, id, nInWidth, nInHeight);
	}
}
template<typename SpriteSheetId>
std::shared_ptr<sf::Texture> ImageContainer<SpriteSheetId>::getSingleTexture(const std::string& name) const{
	auto it = mSingleImageArray.find(name);
	if(it == mSingleImageArray.end()){
		throw std::runtime_error("no such image");
	}
	return it->second.first;
}
template<typename SpriteSheetId>
auto ImageContainer<SpriteSheetId>::getSpriteSheets(const std::string& name) const{
	auto it = mSpriteSheetArray.find(name);
	if(it == mSpriteSheetArray.end()){
		throw std::runtime_error("no such image");
	}
	return it->second;
}

template<typename SpriteSheetId>
void ImageContainer<SpriteSheetId>::clear() {
	mSingleImageArray.clear();
	mSpriteSheetArray.clear();
}