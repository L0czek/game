#pragma once
#include <SFML/Network.hpp>
#include <list>
#include <optional>
#include "Box2D/Box2D.h"

struct Packet{
	virtual sf::Packet serialize() const=0;
	virtual bool deserialize(sf::Packet& ) =0;
};

struct HelloPacket :public Packet{	
	std::string mName;
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "HELO"; };
};

struct ExitPacket :public Packet{
	std::string mName;
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "EXIT"; };
};

struct LoadLevel :public Packet{
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "LOAD"; };
};

struct LevelLoaded :public Packet{
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "LEND"; };
};

struct StartGame :public Packet{
	sf::Vector2i mPlayerPosition;
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "RUNG"; };
};

struct DynamicEntitySyncPacket :public Packet{
	sf::Uint64 mId;
	b2Vec2 mPosition;
	b2Vec2 mVelocity;
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "DSYN"; };
};

struct EnemySyncPacket :public Packet{
	sf::Uint64 mId;
	b2Vec2 mPosition;
	b2Vec2 mVelocity;
	std::string mImage;
	sf::Vector2u mSpriteCordinates;
	sf::Vector2u mWeaponSpriteConrdinates;
	sf::Vector2f mScale;
	sf::Uint64 mState;
	float mHealth;
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "ESYN"; };
};

struct PlayerSyncPacket :public Packet{
	std::string mId;
	b2Vec2 mPosition;
	b2Vec2 mVelocity;
	std::string mImage;
	sf::Vector2u mSpriteCordinates;
	sf::Vector2f mScale;
	float mHealth;
	sf::Uint64 mEquippedWeapon;
	bool mChangeHealth = true;
	std::list<std::pair<sf::Vector2f, sf::Vector2f>> mAttackRequests;
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "PSYN"; };
};

struct PlayerSet :public Packet{
	sf::Vector2u mPosition;
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "PSET"; };
};

struct PlayerList :public Packet{
	std::vector<std::pair<std::string, sf::Vector2u>> mPlayers;
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "PLST"; };
};

struct LevelReset :public Packet{
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "LRST"; };
};

struct NextLevel :public Packet{
	sf::Packet serialize() const override;
	bool deserialize(sf::Packet& packet) override;
	static std::string getId() { return "NXTL"; };
};