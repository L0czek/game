#pragma once

#include <functional>
#include <list>

template<typename _Event>
class EventSource{
	std::list<std::function<void(const _Event&)>> mListeners;
public:
	template<typename _Callback>
	void AddListener(const _Callback& Callback);
	void Notify(const _Event& Event) const;
};

template<typename _Event> 
template<typename _Callback> 
void EventSource<_Event>::AddListener(const _Callback& Callback) {
	mListeners.emplace_back(Callback);
}

template<typename _Event>
void EventSource<_Event>::Notify(const _Event& Event) const{
	for(const auto &i : mListeners){
		i(Event);
	}
}

class GlobalEvents{
	std::list<std::function<void(char)>> mTextCallbacks;
	GlobalEvents() = default;
public:
	static GlobalEvents& getInstance() {
		static GlobalEvents mGlobalEvents;
		return mGlobalEvents;
	}
	template<typename Callback>
	void addTextEventCallback(Callback callback) {
		mTextCallbacks.emplace_back(std::move(callback));
	}
	void notifyText(char c) {
		for(const auto &i : mTextCallbacks){
			i(c);
		}
	}
	void clearText() {
		mTextCallbacks.clear();
	}
};