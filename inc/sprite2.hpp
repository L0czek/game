#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Box2D/Box2D.h"

#include <unordered_map>
#include <memory>
#include <functional>
#include <tuple>
#include "scale.hpp"
#include "common.hpp"
#include "fixtureContainer.hpp"
#include "imageContainer.hpp"

struct FixtureData{
	float mDensity;
	float mFriction;
	float mRestitution;
};


template<typename SpriteSheetId,typename FixtureContainer>
class Sprite :public sf::Drawable, public FixtureContainer{
public:
	typedef std::unordered_map<SpriteSheetId,Image> SpriteSheetData;
	typedef typename SpriteSheetData::iterator SpriteIterator;
	typedef ImageContainer<SpriteSheetId> imageContainer;
private:
	b2World& mWorld;
	UniqueBody mBody;
	FixtureData mFixtureData;
	std::shared_ptr<std::unordered_map<SpriteSheetId,Image>> mSpriteSheet;
	SpriteIterator mSelectedImage;
	SpriteIterator mSpriteSheetEnd;
	std::shared_ptr<sf::Texture> mTexture;
	sf::Sprite mSprite;
	sf::Vector2i mDisplacementVector = {0, 0};
	sf::Clock mClock;
	Animation mAnimation;
public:
	Sprite(b2World& world,const b2BodyDef& bodyDef,std::shared_ptr<sf::Texture> texture = std::make_shared<sf::Texture>());
	Sprite(b2World& world,const b2BodyDef& bodyDef,const b2FixtureDef& fixture,std::shared_ptr<sf::Texture> texture = std::make_shared<sf::Texture>());
	Sprite(const Sprite&) = delete;
	Sprite(Sprite&&) = default;
	~Sprite() {
		if constexpr(std::is_same<FixtureContainer, SingleFixture>::value){
			FixtureContainer::removeFixture();
		}else{
			FixtureContainer::cleanFixtures();
		}
	}

	void setFixtureData(float mDensity,float mFriction,float mRestitution);
	void loadFromFile(const std::string& name, const SpriteSheetId& id,std::size_t nInWidth, std::size_t nInHeight);
	void loadFromFiles(std::initializer_list<std::tuple<std::string,SpriteSheetId,std::size_t,std::size_t>> list);
	void nextChunk();
	void prevChunk();
	void setChunk(std::size_t x=0, std::size_t y=0);
	const sf::Vector2u& getAnimationCordinates() const;
	void setTextue(const std::shared_ptr<sf::Texture>& texture);
	void setImage(const SpriteSheetId& id);
	SpriteSheetId getSelectedImageId() const;

	void setPosition(std::size_t x,std::size_t y);
	sf::Vector2f getPosition() const;
	void scaleTo(std::size_t x,std::size_t y);
	sf::Vector2f getScale() const{ return mSprite.getScale(); }
	void setScale(const sf::Vector2f& scale) { mSprite.setScale(scale); }
	void resizeFixture(std::size_t x,std::size_t y);
	void resize(std::size_t x,std::size_t y);
	void setRotation(float angle);

	void setLastChunk(const SpriteSheetId& id,const sf::Vector2u& cordinates);
	void runAnimation(float deltaTime, bool repeat = true);
	void runReversedAnimation(float deltaTime, bool repeat = true);
	bool animationIsRunning() const;
	void stopAnimation();
	void animationStep();
	void setOriginToMiddle();
	bool isImageSelected(const SpriteSheetId& id) const;
	void setUserData(void* ptr);
	sf::Sprite& getSprite() { return mSprite; }

	void resetDisplacement() { mDisplacementVector = {0, 0}; }
	const UniqueBody& getBody() const;
	const std::shared_ptr<sf::Texture>& getTexture() const;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void update();

	void setSpriteSheet(std::shared_ptr<SpriteSheetData> ptr = std::make_shared<SpriteSheetData>()) { mSpriteSheet = ptr; }
	template<typename T = FixtureContainer,typename std::enable_if_t<std::is_same<T, MultiFixture>::value,int> = 0>
	auto addFixture(const b2FixtureDef& fixture) { return FixtureContainer::addFixture(mBody, fixture); }
};

using SingleStringSprite = Sprite<std::string,SingleFixture>;
using MultiStringSprite = Sprite<std::string,MultiFixture>;

template<typename SpriteSheetId,typename FixtureContainer>
SpriteSheetId Sprite<SpriteSheetId,FixtureContainer>::getSelectedImageId() const{
	if(mSelectedImage != mSpriteSheetEnd){
		return mSelectedImage->first;
	}
	return "";
}
template<typename SpriteSheetId,typename FixtureContainer>
const sf::Vector2u& Sprite<SpriteSheetId,FixtureContainer>::getAnimationCordinates() const{
	return mAnimation.mPositionInCordinates;
}
template<typename SpriteSheetId,typename FixtureContainer>
Sprite<SpriteSheetId,FixtureContainer>::Sprite(b2World& world,const b2BodyDef& bodyDef,std::shared_ptr<sf::Texture> texture) :mWorld(world), mTexture(texture){
	mBody = UniqueBody(mWorld.CreateBody(&bodyDef), std::bind(&b2World::DestroyBody, &mWorld, std::placeholders::_1));
	if(!mBody){
		throw std::runtime_error("Can not create body.");
	}	
	mSprite.setTexture(*mTexture);
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	target.draw(mSprite, states);
}
template<typename SpriteSheetId,typename FixtureContainer>
Sprite<SpriteSheetId,FixtureContainer>::Sprite(b2World& world,const b2BodyDef& bodyDef,const b2FixtureDef& fixture,std::shared_ptr<sf::Texture> texture) :Sprite(world, bodyDef, texture){
	if constexpr(std::is_same<FixtureContainer, SingleFixture>::value){
		FixtureContainer::setFixture(mBody, fixture);
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setFixtureData(float mDensity,float mFriction,float mRestitution) {
	mFixtureData.mDensity = mDensity;
	mFixtureData.mFriction = mFriction;
	mFixtureData.mRestitution = mRestitution;
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setLastChunk(const SpriteSheetId& id,const sf::Vector2u& cordinates) {
	auto it = mSpriteSheet->find(id);
	if(it != mSpriteSheetEnd){
		it->second.mMaxCordinates = cordinates;
	}else{
		throw std::runtime_error(Concat("No such image in prite sheet ", id));
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::loadFromFile(const std::string& name, const SpriteSheetId& id,std::size_t nInWidth, std::size_t nInHeight) {
	sf::Image image;
	if(!image.loadFromFile(name)){
		throw std::runtime_error("Can not load image.");
	}
	sf::Vector2u size = image.getSize();
	sf::Vector2u singleSize = size;
	singleSize.x /= nInWidth;
	singleSize.y /= nInHeight;
	mSpriteSheet->insert(std::make_pair(id, Image{ std::move(image), singleSize, sf::Vector2u(0,0), size, nInWidth, nInHeight }));
	mSpriteSheetEnd = mSpriteSheet->end();
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setImage(const SpriteSheetId& id) {
	auto it = mSpriteSheet->find(id);
	if(it == mSpriteSheetEnd){
		throw std::runtime_error("not such image.");
	}
	if(mSelectedImage == mSpriteSheetEnd){
		if constexpr(std::is_same<FixtureContainer, SingleFixture>::value){
			resizeFixture(it->second.mSingleSize.x, it->second.mSingleSize.y);
		}
		setOriginToMiddle();
	}else if(it->second.mSingleSize != mSelectedImage->second.mSingleSize){
		mDisplacementVector.y += (static_cast<int>(mSelectedImage->second.mSingleSize.y) - static_cast<int>(it->second.mSingleSize.y));
	}
	mSelectedImage = it;
	if(mTexture->getSize() < it->second.mImage.getSize()){
		auto size = it->second.mImage.getSize();
		mTexture->create(size.x, size.y);
	}
	mTexture->update(it->second.mImage);
	setChunk(0, 0);
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::resizeFixture(std::size_t x,std::size_t y) {
	b2PolygonShape shape;
	shape.SetAsBox(Scale::SFML2Box(x/2.0),Scale::SFML2Box(y/2.0));
	b2FixtureDef fixture;
	fixture.shape = &shape;
	fixture.density = mFixtureData.mDensity;
	fixture.friction = mFixtureData.mFriction;
	fixture.restitution = mFixtureData.mRestitution;
	FixtureContainer::setFixture(mBody, fixture);
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::resize(std::size_t x,std::size_t y) { 
	if constexpr(std::is_same<FixtureContainer, SingleFixture>::value){
		resizeFixture(x, y);
	}
	scaleTo(x, y);
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setTextue(const std::shared_ptr<sf::Texture>& texture) {
	mTexture = texture;
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::nextChunk() {
	if(mSelectedImage != mSpriteSheetEnd){
		std::size_t x = mAnimation.mPositionInCordinates.x + 1;
		std::size_t y = mAnimation.mPositionInCordinates.y;
		if(x > mSelectedImage->second.mMaxCordinates.x){
			x = 0;
			y += 1;
			if(y > mSelectedImage->second.mMaxCordinates.y){
				y = 0;
			}
		}
		mAnimation.mPositionInCordinates = sf::Vector2u{ static_cast<unsigned int>(x), static_cast<unsigned int>(y)};
		mSelectedImage->second.mPosition = mAnimation.mPositionInCordinates * mSelectedImage->second.mSingleSize;
		mSprite.setTextureRect(sf::IntRect{convert<int>(mSelectedImage->second.mPosition), convert<int>(mSelectedImage->second.mSingleSize)});
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::prevChunk() {
	if(mSelectedImage != mSpriteSheetEnd){
		std::size_t x = mSelectedImage->second.mPosition.x;
		std::size_t y = mSelectedImage->second.mPosition.y;
		if(x == 0){
			x = mSelectedImage->second.mTotalSize.x - mSelectedImage->second.mSingleSize.x;
			if(y == 0){
				y = mSelectedImage->second.mTotalSize.y - mSelectedImage->second.mSingleSize.y;
			}else{
				y -= mSelectedImage->second.mSingleSize.y;
			}
		}else{
			x -= mSelectedImage->second.mSingleSize.x;
		}
		mSelectedImage->second.mPosition = convert<unsigned int>(x, y);
		mSprite.setTextureRect(sf::IntRect{convert<int>(mSelectedImage->second.mPosition), convert<int>(mSelectedImage->second.mSingleSize)});
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setChunk(std::size_t x, std::size_t y) {
	if(mSelectedImage != mSpriteSheetEnd){
		mSelectedImage->second.mPosition = mSelectedImage->second.mSingleSize * convert<unsigned int>(x, y);
		mSprite.setTextureRect(sf::IntRect{convert<int>(mSelectedImage->second.mPosition), convert<int>(mSelectedImage->second.mSingleSize)});
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setPosition(std::size_t x,std::size_t y) {
	mSprite.setPosition(x, y);
	mBody->SetTransform(b2Vec2{Scale::SFML2Box(x), Scale::SFML2Box(y)}, mBody->GetAngle());
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::scaleTo(std::size_t x,std::size_t y) {
	sf::Vector2f scale{1, 1};
	if(mSelectedImage != mSpriteSheetEnd){
		scale = convert<float>(x, y) / mSelectedImage->second.mSingleSize;
	}else{
		scale = convert<float>(x, y) / mTexture->getSize();
	}
	mSprite.setScale(scale);
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setRotation(float angle) {
	mSprite.setRotation(angle);
	mBody->SetTransform(mBody->GetPosition(), angle);
}
template<typename SpriteSheetId,typename FixtureContainer>
sf::Vector2f Sprite<SpriteSheetId,FixtureContainer>::getPosition() const{
	return mSprite.getPosition();
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::runAnimation(float deltaTime, bool repeat) {
	if(mSelectedImage != mSpriteSheetEnd){
		mAnimation.mDeltaTime = deltaTime;
		mAnimation.mRepeat = repeat;
		mAnimation.mReversed = false;
		mAnimation.mIsRunning = true;
		mAnimation.mPositionInCordinates = sf::Vector2u{0, 0};
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::runReversedAnimation(float deltaTime, bool repeat) {
	if(mSelectedImage != mSpriteSheetEnd){
		mAnimation.mDeltaTime = deltaTime;
		mAnimation.mRepeat = repeat;
		mAnimation.mReversed = true;
		mAnimation.mIsRunning = true;
		mAnimation.mPositionInCordinates = mSelectedImage->second.mMaxCordinates;
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
bool Sprite<SpriteSheetId,FixtureContainer>::animationIsRunning() const{
	return mSelectedImage != mSpriteSheetEnd && mAnimation.mIsRunning;
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::stopAnimation() {
	if(mSelectedImage != mSpriteSheetEnd){
		mSelectedImage->mAnimation.mIsRunning = false;
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::animationStep() {
	if(mSelectedImage != mSpriteSheetEnd && mAnimation.mIsRunning && mClock.getElapsedTime().asSeconds() >= mAnimation.mDeltaTime){
		if(mAnimation.mReversed){
			prevChunk();
		}else{
			nextChunk();
		}
		mClock.restart();
		if(!mAnimation.mRepeat){
			if(!mAnimation.mReversed && mAnimation.mPositionInCordinates == mSelectedImage->second.mMaxCordinates)
				mAnimation.mIsRunning = false;
			else if(mAnimation.mReversed && mSelectedImage->second.mPosition == sf::Vector2u{0, 0})
				mAnimation.mIsRunning = false;
		}
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::update() {
	mSprite.setPosition(Scale::Box2SFML<float>(mBody->GetPosition()) + convert<float>(mDisplacementVector) * mSprite.getScale().y);
}
template<typename SpriteSheetId,typename FixtureContainer>
const UniqueBody& Sprite<SpriteSheetId,FixtureContainer>::getBody() const{
	return mBody;
}
template<typename SpriteSheetId,typename FixtureContainer>
const std::shared_ptr<sf::Texture>& Sprite<SpriteSheetId,FixtureContainer>::getTexture() const{
	return mTexture;
}
template<typename SpriteSheetId,typename FixtureContainer>
bool Sprite<SpriteSheetId,FixtureContainer>::isImageSelected(const SpriteSheetId& id) const{
	return mSelectedImage != mSpriteSheetEnd && mSelectedImage->first == id;
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setOriginToMiddle() {
	if(mSelectedImage != mSpriteSheetEnd){
		mSprite.setOrigin(mSelectedImage->second.mSingleSize / sf::Vector2f{2, 2});
	}else{
		mSprite.setOrigin(mTexture->getSize() / sf::Vector2f{2, 2});
	}
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::setUserData(void* ptr) {
	mBody->SetUserData(ptr);
}
template<typename SpriteSheetId,typename FixtureContainer>
void Sprite<SpriteSheetId,FixtureContainer>::loadFromFiles(std::initializer_list<std::tuple<std::string,SpriteSheetId,std::size_t,std::size_t>> list) {
	for(const auto &i : list){
		auto [ name, id, nx, ny] = i;
		loadFromFile(name, id, nx, ny);
	}
}