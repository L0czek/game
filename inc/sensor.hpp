#pragma once
#include "common.hpp"
#include "contactListener.hpp"
#include <functional>
#include "collisions.hpp"

class Platform;
class Lift;
class Player;


class Sensor{
	std::size_t mContacts = 0;
protected:
	void inc() { mContacts++; }
	void dec() { mContacts--; }
public:
	virtual ~Sensor() {}
	bool isInContact() const{ return mContacts > 0; }
	void clearFlag() { mContacts = 0; }
};

template<typename... ContactList>
class ContactSensor :public ContactInterface, public Sensor{
	b2Body& mBody;
	UniqueFixture mFixture;
	std::function<void(Platform&)>  mPlatformCallback;
	std::function<void(Lift&)> mLiftCallback;
	std::function<void(LaserFly&)> mLaserFlyCallback;
	std::function<void(Player&)> mPlayerCallback;
	std::function<void(Sensor&)> mSensorCallback;

public:
	ContactSensor(b2Body& body,sf::Vector2i position,sf::Vector2i size) :mBody(body){
		b2PolygonShape shape;
		shape.SetAsBox(Scale::SFML2Box(size.x), Scale::SFML2Box(size.y), b2Vec2{Scale::SFML2Box(position.x), Scale::SFML2Box(position.y)}, 0);
		b2FixtureDef fixture;
		fixture.shape = &shape;
		fixture.isSensor = true;
		mFixture = UniqueFixture(mBody.CreateFixture(&fixture), [this](b2Fixture* ptr){ mBody.DestroyFixture(ptr); });
		//::setCollisionFilter(*mFixture, ObjectType::Sensor, ::collideWithAll());
		mFixture->SetUserData(this);
		clearFlag();
	}
	void BeginContact(Platform & entity) override {
		if constexpr(is_in_pack<Platform, ContactList...>::value) { 
	  		inc();
	  		if(mPlatformCallback){
	  			mPlatformCallback(entity);
	  		} 
	 	}
	}
	void EndContact(Platform &) override {
		if constexpr(is_in_pack<Platform, ContactList...>::value) { 
			dec(); 
		}
	}
	
	void BeginContact(Player & entity) override {
		if constexpr(is_in_pack<Player, ContactList...>::value) { 
	  		inc();
	  		if(mPlayerCallback){
	  			mPlayerCallback(entity);
	  		} 
	 	}
	}
	void EndContact(Player &) override {
	  	if constexpr(is_in_pack<Player, ContactList...>::value) { 
	  		dec(); 
	  	}
	}
	
	void BeginContact(Lift & entity) override {
	  	if constexpr(is_in_pack<Lift, ContactList...>::value) { 
	  		inc();
	  		if(mLiftCallback){
	  			mLiftCallback(entity);
	  		} 
	  	}
	}
	void EndContact(Lift &) override {
	  	if constexpr(is_in_pack<Lift, ContactList...>::value) { 
	  		dec(); 
	  	}
	}
	
	void BeginContact(Sensor & entity) override {
	  	if constexpr(is_in_pack<Sensor, ContactList...>::value) { 
	  		inc();
	  		if(mSensorCallback){
	  			mSensorCallback(entity);
	  		} 
	  	}
	}
	void EndContact(Sensor &) override {
	  	if constexpr(is_in_pack<Sensor, ContactList...>::value) { 
	  		dec(); 
	  	}
	}
	
	void BeginContact(LaserFly & entity) override {
	  	if constexpr(is_in_pack<LaserFly, ContactList...>::value) { 
	  		inc();
	  		if(mLaserFlyCallback){
	  			mLaserFlyCallback(entity);
	  		} 
	  	}
	}
	void EndContact(LaserFly & entity) override {
	  	if constexpr(is_in_pack<LaserFly, ContactList...>::value) { 
	  		dec();
	  	}
	}
	void BeginContact(ContactInterface& entity) override{ entity.BeginContact(static_cast<Sensor&>(*this)); }
	void EndContact(ContactInterface& entity) override{ entity.EndContact(static_cast<Sensor&>(*this)); }
	template<typename Class>
	void onContact(std::function<void(Class&)> callback) {
		if constexpr(std::is_same<Class, Player>::value){
			mPlayerCallback = callback;
		}else if constexpr(std::is_same<Class, Platform>::value){
			mPlatformCallback = callback;
		}else if constexpr(std::is_same<Class, Lift>::value){
			mLiftCallback = callback;
		}else if constexpr(std::is_same<Class, Sensor>::value){
			mSensorCallback = callback;
		}else if constexpr(std::is_same<Class, LaserFly>::value){
			mLaserFlyCallback = callback;
		}
	}
	void turnOff() {
		setCollisionFilter(*mFixture, ObjectType::Sensor, collideWithNone());
	}
	void accept(EntityVisitor&) override{}
};