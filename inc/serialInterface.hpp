#pragma once

#include "common.hpp"
#include <type_traits>
#include <typeinfo>

class SerialInterface{
public:
	virtual std::string serialize() const=0;
	virtual void deserialize(const std::string& data) =0;
};

template<typename T>
void SerializeElement(std::string& data,const T& t) {
	if constexpr(std::is_integral<T>::value || std::is_floating_point<T>::value){
		constexpr uint32_t len = sizeof(T);
		data.append(reinterpret_cast<const char*>(&len), sizeof(len));
		data.append(reinterpret_cast<const char*>(&t), len);
	}else if constexpr(std::is_same<T,std::string>::value){
		constexpr uint32_t len = t.length();
		data.append(reinterpret_cast<const char*>(&len), sizeof(len));
		data.append(t);
	}else{
		//static_assert(false, "Cant Serialize type ");
	}
}

template<typename T>
void SerializeData(std::string& data,const T& t) {
	SerializeElement(data, t);
}

template<typename T,typename... Args>
void SerializeData(std::string& data,const T& t,Args&&... args) {
	SerializeElement(data, t);
	SerializeData(data, std::forward<Args>(args)...);
}


template<typename... Args>
std::string Serialize(Args&&... args) {
	std::string ret;
	SerializeData(ret, std::forward<Args>(args)...);
	return ret;
}

template<typename T>
void DeserializeElement(std::string::const_iterator& it,T& t) {
	if constexpr(std::is_integral<T>::value || std::is_floating_point<T>::value){
		uint32_t len = *reinterpret_cast<const uint32_t*>(&(*it));
		std::advance(it, sizeof(uint32_t));
		t = *reinterpret_cast<const T*>(&(*it));
		std::advance(it, len);
	}else if constexpr(std::is_same<T,std::string>::value){
		uint32_t len = *reinterpret_cast<const uint32_t*>(&(*it));
		std::advance(it, sizeof(uint32_t));
		t = std::string(it, it+len);
		std::advance(it, len);
	}else{
		
	}

}

template<typename T>
void DeserializeData(std::string::const_iterator& it,T& t) {
	DeserializeElement(it, t);
}

template<typename T,typename... Args>
void DeserializeData(std::string::const_iterator& it,T& t,Args&&... args) {
	DeserializeElement(it, t);
	DeserializeData(it, std::forward<Args>(args)...);
}

template<typename... Args>
void Deserialize(const std::string& data,Args&&... args) {
	std::string::const_iterator it = data.cbegin();
	DeserializeData(it, std::forward<Args>(args)...);
}
