#pragma once
#include "game.hpp"
#include "map.hpp"
#include <memory>
#include "sound.hpp"

struct Level1 :public Game::Level{
	std::shared_ptr<sf::Music> mActionMusic;
	bool mLoaded = false;
	sf::Sprite mBackground;
    Level1();
    void load(std::function<void(float)>&& progress) override;
    void operator()(Map& map) override;
    std::function<bool(const Map&,const std::unique_ptr<Player>&)> getGameOverCondition() const override;
	std::function<bool(const Map&,const std::unique_ptr<Player>&)> getGameWinCondition() const override;
	void start() override;
	void stop() override;
};

struct Level2 :public Game::Level{
	std::shared_ptr<sf::Music> mActionMusic;
	sf::Sprite mBackground;
	bool mLoaded = false;
    Level2();
    void load(std::function<void(float)>&& progress) override;
    void operator()(Map& map) override;
    std::function<bool(const Map&,const std::unique_ptr<Player>&)> getGameOverCondition() const override;
	std::function<bool(const Map&,const std::unique_ptr<Player>&)> getGameWinCondition() const override;
	void start() override;
	void stop() override;
};