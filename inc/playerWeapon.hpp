#pragma once

#include <cstdint>
#include "Box2D/Box2D.h"
#include <SFML/Graphics.hpp>
#include "sound.hpp"

class AK47{
	std::size_t mAmmoLeftInMagazine;
	std::size_t mTotalAmmo;
	std::size_t mAmmoCapacity;
	b2World& mWorld;
	sf::Sound mSound;
public:
	AK47(b2World& world) :mWorld(world) {
		mSound.setBuffer(SoundContainer::getInstance().getSoundBuffer("ak47"));
	}
	void shoot(const sf::Vector2f& position,const sf::Vector2f& direction);
	void reload();
};

class BaseBallStick{
	b2World& mWorld;
	sf::Sound mSound;
public:
	BaseBallStick(b2World& world) :mWorld(world) {
		mSound.setBuffer(SoundContainer::getInstance().getSoundBuffer("baseball"));
	}
	void attack(const sf::Vector2f& position,const sf::Vector2f& direction);
};