#pragma once

#include <SFML/Graphics.hpp>
#include "Box2D/Box2D.h"

#include "common.hpp"
#include <set>
#include <unordered_map>
#include <vector>
#include <memory>
#include <list>
#include <functional>
#include "entity.hpp"
#include "contactListener.hpp"

class NetworkServer;
class NetworkClient;
class Player;

class Map :public sf::Drawable{
public:
	class EntityFactory{
	public:
		virtual char id() const=0;
		virtual bool canConsolidate() const =0;
		virtual void make(Map&,const sf::Vector2u&,const sf::Vector2u&) =0;
	};
private:
	static const int velocityIterations;   //how strongly to correct velocity
    static const int positionIterations;
    sf::Sprite mBackGround;
    sf::Clock mClock;
	sf::View mView;
	b2World mWorld;
	ContactListener mContactListener;
	bool mScriptsEnabled;
	std::unique_ptr<Player> mPlayer;
	std::set<std::unique_ptr<Entity>> mStaticEntities;
	std::set<std::unique_ptr<DynamicEntity>> mDynamicEntities;
	std::set<std::unique_ptr<Enemy>> mEnemies;
	std::list<sf::Vector2u> mPlayerSpawns;
	std::vector<std::unique_ptr<Player>> mNetworkPlayers;
	std::vector<std::pair<std::function<bool(const std::unique_ptr<Entity>&)>,std::function<void(Map&,const std::unique_ptr<Entity>&)>>> mStaticEntitiesScripts;
	std::vector<std::pair<std::function<bool(const std::unique_ptr<DynamicEntity>&)>,std::function<void(Map&,const std::unique_ptr<DynamicEntity>&)>>> mStaticDynamicScripts;
	std::vector<std::pair<std::function<bool(const std::unique_ptr<Entity>&)>,std::function<void(Map&,const std::unique_ptr<Entity>&)>>> mEnemiesScripts;
	std::list<std::pair<std::function<bool(const Map&,const std::unique_ptr<Player>&)>,std::function<void(Map&,const std::unique_ptr<Player>&)>>> mPlayerScripts;

	template<std::size_t i,typename... Tags>
	void iterate(std::tuple<Tags...>& tags,std::map<char,std::pair<std::function<void(Map&,const sf::Vector2u&,const sf::Vector2u&)>,bool>>& map) {
		auto & element = std::get<i>(tags);
		map.insert(std::make_pair(element.id(), std::make_pair(std::bind(&EntityFactory::make, &element, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3), element.canConsolidate())));
		if constexpr(i < sizeof...(Tags)-1){
			iterate<i+1,Tags...>(tags, map);
		}
	}
	void updatePlayer();
public:
	typedef std::set<std::unique_ptr<Entity>>::iterator iterator;
	typedef std::set<std::unique_ptr<LivingEntity>>::iterator livingIterator;
	typedef std::set<std::unique_ptr<DynamicEntity>>::iterator dynamicIterator;
	typedef std::vector<std::pair<std::function<bool(const std::unique_ptr<Entity>&)>,std::function<void(Map&,const std::unique_ptr<Entity>&)>>>::iterator scriptIterator;
	typedef std::list<std::pair<std::function<bool(const Map&,const std::unique_ptr<Player>&)>,std::function<void(Map&,const std::unique_ptr<Player>&)>>>::iterator playerScriptIterator;
	Map(const sf::FloatRect& initialViewport,const b2Vec2& gravity = b2Vec2{0, 9.81});
	void createPlayer(const sf::Vector2u& position);
	void destroyPlayer();
	std::set<std::unique_ptr<Entity>>& getStaticEntities() { return mStaticEntities; }
	std::set<std::unique_ptr<DynamicEntity>>& getDynamicEntities() { return mDynamicEntities; }	
	std::set<std::unique_ptr<Enemy>>& getEnemies() { return mEnemies; }
	void addSpawnPoint(const sf::Vector2u& position) { mPlayerSpawns.emplace_back(position); }
	sf::Vector2u getSpawnPoint() { if(mPlayerSpawns.empty()) { return {0,0}; } auto ret = mPlayerSpawns.back(); mPlayerSpawns.pop_back(); return ret;}
	iterator addStaticEntity(std::unique_ptr<Entity> entity);
	dynamicIterator addDynamicEntity(std::unique_ptr<DynamicEntity> entity);
	auto addEnemy(std::unique_ptr<Enemy> entity);
	void addNetworkPlayer(const std::string& name, const sf::Vector2u& pos,bool onHost);
	bool areAllPlayersDead() const;
	void disableScripts() { mScriptsEnabled = false; }
	void enableScripts() { mScriptsEnabled = true; }
	template<typename Class,typename... Args>
	auto addStaticEntity(Args&&... args) {
		return std::get<0>(mStaticEntities.insert(std::make_unique<Class>(mWorld, std::forward<Args>(args)...)));
	}
	template<typename Class,typename... Args>
	auto addDynamicEntity(Args&&... args) {
		return std::get<0>(mDynamicEntities.insert(std::make_unique<Class>(mWorld, std::forward<Args>(args)...)));
	}
	template<typename Class,typename... Args>
	auto addEnemy(Args&&... args) {
		return std::get<0>(mEnemies.insert(std::make_unique<Class>(mWorld, std::forward<Args>(args)...)));
	}

	template<typename Condition,typename Script>
	playerScriptIterator addPlayerScript(Condition condition,Script script) {
		return mPlayerScripts.insert(mPlayerScripts.end(),std::make_pair(condition, script));
	}
	template<typename Condition,typename Script>
	scriptIterator addStaticEntityScript(Condition condition,Script script) {
		return mStaticEntitiesScripts.insert(mPlayerScripts.end(),std::make_pair(condition, script));
	}
	template<typename Condition,typename Script>
	scriptIterator addDynamicEntityScript(Condition condition,Script script) {
		return mStaticDynamicScripts.insert(mPlayerScripts.end(),std::make_pair(condition, script));
	}
	template<typename Condition,typename Script>
	scriptIterator addEnemiesScript(Condition condition,Script script) {
		return mEnemiesScripts.insert(mPlayerScripts.end(),std::make_pair(std::move(condition), std::move(script)));
	}


	void clearPlayerScripts() { mPlayerScripts.clear(); }
	void removeStaticEntity(iterator it);
	void removeDynamicEntity(dynamicIterator it);
	//void removeEnemy(livingIterator it);

	void removeStaticEntityScript(scriptIterator it);
	//void removeDynamicEntityScript(scriptIterator it);
	//void removeEnemyScript(scriptIterator it);
	void removePlayerScript(playerScriptIterator it);

	void clearStaticEntity();
	void clearDynamicEntity();
	void clearEnemy();
	void clearAll();

	bool areAllEnemiesDead() const;

	sf::View& getView() { return mView; }
	const std::unique_ptr<Player>& getPlayer() { return mPlayer; }
	void updateWindow(sf::RenderWindow& window) const;
	void platformsFromString(const std::string& content,char chunk,const sf::Vector2u& chunkSize,const std::shared_ptr<sf::Texture>& texture);

	// requires:
	/*
	struct {
		char operator()(); => return char which identyfies class
		void operator()(Map& map);	=> adds object to map
	}
	*/

	template<typename... Tags>
	void printf(const std::string& string,const sf::Vector2u& chunkSize,Tags&&... customTags) {
		 std::tuple<Tags...> tags{ std::forward<Tags>(customTags)... };
		 std::map<char, std::pair<std::function<void(Map&,const sf::Vector2u&,const sf::Vector2u&)>,bool>> lookup;
		 iterate<0,Tags...>(tags, lookup);
		 unsigned int x = 0;
		 unsigned int y = 0;
		 sf::Vector2u offset = {chunkSize.x / 2, 0};
		 for(std::size_t i=0; i < string.length(); ++i){
		 	char c = string[i];
		 	if(c != '\n'){
		 		auto it = lookup.find(c);
		 		if(it != lookup.end()){
		 			if(it->second.second){
		 				unsigned int start = x;
		 				unsigned int n = 1;
		 				while(string[++i] == c){
		 					x++;
		 					n++;
		 				}
		 				if(i > 0){
		 					i--;
		 				}
		 				if(n == 1){
		 					auto position = sf::Vector2u{x, y} * chunkSize + chunkSize / 2;
		 					it->second.first(*this, position, chunkSize);
		 				}else{
		 					auto size = sf::Vector2u{n, 0} * chunkSize;
		 					auto position = sf::Vector2u{start, y} * chunkSize;
		 					position.x += (chunkSize.x * n)/2;
		 					position.y += chunkSize.y/2;
		 					it->second.first(*this, position, size);
		 				}
		 			}else{
		 				auto position = sf::Vector2u{x, y} * chunkSize + chunkSize / 2;
		 				it->second.first(*this, position, chunkSize);
		 			}
		 		}
		 		x++;
		 	}else{
		 		x = 0;
		 		y++;
		 	}
		 }
	}

	void update();
	void load();
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void setBackground(sf::Sprite sprite) {
		mBackGround = std::move(sprite);
	}
	void updateNetworkState(NetworkServer& server);
	void updateNetworkState(NetworkClient& client);
};