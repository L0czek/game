#pragma once

#include <SFML/Graphics.hpp>
#include "map.hpp"
#include "player.hpp"
#include <functional>
#include "gameConfig.hpp"
#include "network.hpp"

class Game{
public:
	class Screen :public sf::Drawable{
		Game& mGame;
		sf::RenderWindow& mWindow;
	protected:
		Game& getGame() { return mGame; }
		sf::RenderWindow& getWindow() { return mWindow; }
	public:
		Screen(Game& mGame,sf::RenderWindow& mWindow) :mGame(mGame), mWindow(mWindow) {}
		virtual ~Screen() {}
		virtual void create() =0;
		virtual void update() =0;
	};
	class LoadingScreen :public Screen{
	public:
		LoadingScreen(Game& mGame,sf::RenderWindow& mWindow) :Screen(mGame, mWindow){}
		virtual void updateProgress(float) =0;
	};
	class Level{
	public:
		virtual void load(std::function<void(float)>&&) =0;
		virtual void operator()(Map&) =0;
		virtual std::function<bool(const Map&,const std::unique_ptr<Player>&)> getGameOverCondition() const=0;
		virtual std::function<bool(const Map&,const std::unique_ptr<Player>&)> getGameWinCondition() const=0;
		virtual void start() =0;
		virtual void stop() =0;
	};
private:
	enum class DrawingState{
		None,
		InitialScreen,
		Map,
		GameOverScreen,
		GameWinScreen,
		LoadingScreen,
		HostScreen,
		ClientScreen,
		MapHost,
		MapClient
	}mDrawingState = DrawingState::None;
	enum class NetworkMode{
		None,
		Host,
		Client
	}mNetworkMode;
	GameConfig mConfig;
	sf::RenderWindow mWindow;
	
	std::vector<std::unique_ptr<Level>> mLevels;
	std::size_t mSelectedLevel;
	std::unique_ptr<Map> mMap;
	
	std::unique_ptr<Screen> mInitialScreen;
	
	std::vector<std::unique_ptr<LoadingScreen>> mLoadingScreens;
	std::size_t mLoadingScreenToDisplay;
	
	std::unique_ptr<Screen> mGameOverScreen;
	std::unique_ptr<Screen> mGameWinScreen;
	std::unique_ptr<Screen> mGameMultiHostScreen;
	std::unique_ptr<Screen> mGameMultiClientScreen;

	Map::playerScriptIterator mGameOverScript;
	Map::playerScriptIterator mGameWinScript;

	std::unique_ptr<NetworkServer> mNetworkServer;
	std::unique_ptr<NetworkClient> mNetworkClient;

	sf::Sprite mBsod;
	std::string mNick;
	void gameOver(Map& map,const std::unique_ptr<Player>& player);
	void gameWin(Map& map,const std::unique_ptr<Player>& player);
public:
	Game(const GameConfig& config);
	void setupLevelHost(std::size_t level, std::size_t loadingScreen);
	void setupLevelClient(std::size_t level, std::size_t loadingScreen);
	void setupLevelSingle(std::size_t level, std::size_t loadingScreen);
	template<typename Level>
	void createMap(Level& level,const sf::FloatRect& mRect) {
		mConfig.mInitialViewport = mRect;
		createMap<Level>(level);
	}
	template<typename Level>
	void createMap(Level& level) {
		mMap = std::make_unique<Map>(mConfig.mInitialViewport, mConfig.mGravity);
		level(*mMap);
	}
	void allocateMap() {
		mMap = std::make_unique<Map>(mConfig.mInitialViewport, mConfig.mGravity);
	}
	template<typename LevelClass,typename... Args>
	void addLevel(Args&&... args) {
		mLevels.emplace_back(std::make_unique<LevelClass>(std::forward<Args>(args)...));
	}
	template<typename LevelClass>
	void addLevel() {
		mLevels.emplace_back(std::make_unique<LevelClass>());
	}
	void loop();
	void multiHostLoop();
	void multiClientLoop();
	Map& getMap();

	template<typename InitialScreen,typename... Args>
	void createInitialScreen(Args&&... args) {
		mInitialScreen = std::make_unique<InitialScreen>(*this, mWindow, std::forward<Args>(args)...);
	}

	template<typename LoadingScreen,typename... Args>
	void addLoadingScreen(Args&&... args) {
		mLoadingScreens.emplace_back(std::make_unique<LoadingScreen>(*this, mWindow, std::forward<Args>(args)...));
	}

	template<typename GameOverScreen,typename... Args>
	void createGameOverScreen(Args&&... args) {
		mGameOverScreen = std::make_unique<GameOverScreen>(*this, mWindow, std::forward<Args>(args)...);
	}

	template<typename GameWinScreen,typename... Args>
	void createGameWinScreen(Args&&... args) {
		mGameWinScreen = std::make_unique<GameWinScreen>(*this, mWindow, std::forward<Args>(args)...);
	}
	template<typename GameMultiHostScreen,typename... Args>
	void createGameMultiHostScreen(Args&&... args) {
		mGameMultiHostScreen = std::make_unique<GameMultiHostScreen>(*this, mWindow, std::forward<Args>(args)...);
	}
	template<typename GameMultiClientScreen,typename... Args>
	void createGameMultiClientScreen(Args&&... args) {
		mGameMultiClientScreen = std::make_unique<GameMultiClientScreen>(*this, mWindow, std::forward<Args>(args)...);
	}


	template<typename GameOverCondition>
	void setGameOverCondition(GameOverCondition callback) {
		if(!mMap){
			throw std::runtime_error("Error map wasnt created");
		}
		mGameOverScript = mMap->addPlayerScript(callback, std::bind(&Game::gameOver, this, std::placeholders::_1, std::placeholders::_2));
	}

	template<typename GameWinCondition>
	void setGameWinCondition(GameWinCondition callback) {
		if(!mMap){
			throw std::runtime_error("Error map wasnt created");
		}
		mGameWinScript = mMap->addPlayerScript(std::move(callback), std::bind(&Game::gameWin, this, std::placeholders::_1, std::placeholders::_2));
	}

	void loadLevel(std::size_t levelIndex,std::size_t loadingScreenId);
	
	void displayInitialScreen() { mInitialScreen->create(); mDrawingState = DrawingState::InitialScreen; }
	void startGame();
	bool isNextLevel();
	void nextLevel();
	void bsod();
	void setNick(const std::string& nick) {
		mNick = nick;
	}
	const std::string& getNick() const{ return mNick; }
	Level& getSelectedLevel() { return *mLevels[mSelectedLevel]; }
	void displayHostScreen() { mGameMultiHostScreen->create(); mDrawingState = DrawingState::HostScreen; }
	void displayClientScreen() { mGameMultiClientScreen->create(); mDrawingState = DrawingState::ClientScreen; }
	void startServer() { mNetworkServer = std::make_unique<NetworkServer>(mConfig.mPort, mNick); mNetworkMode = NetworkMode::Host;}
	void startClient() { mNetworkClient = std::make_unique<NetworkClient>(mNick); mNetworkMode = NetworkMode::Client;}
	const GameConfig& getConfig() const{ return mConfig; }
	NetworkServer& getServer() { return *mNetworkServer; }
	NetworkClient& getClient() { return *mNetworkClient; }
	bool isInNetworkMode() const{ return mNetworkMode != NetworkMode::None; }
	bool isHost() const{ return mNetworkMode == NetworkMode::Host; }
	bool isClient() const{ return mNetworkMode == NetworkMode::Client; }
	std::size_t getLevelIndex() const{ return mSelectedLevel; }
	std::size_t getLoadingScreenIndex() const{ return mLoadingScreenToDisplay; }
	~Game();
	void resolvEvents();
};