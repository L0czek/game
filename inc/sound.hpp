#pragma once

#include <map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <memory>

class SoundContainer{
	std::map<std::string, sf::SoundBuffer> mSoundBuffer;
	std::map<std::string, std::shared_ptr<sf::Music>> mMusic;
	SoundContainer() {}
public:
	static SoundContainer& getInstance();
	sf::SoundBuffer& getSoundBuffer(const std::string& name);
	std::shared_ptr<sf::Music> getMusic(const std::string& name);
	void loadSoundBuffer(const std::string& id,const std::string& fileName);
	void loadMusic(const std::string& id,const std::string& fileName);
};