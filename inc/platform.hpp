#pragma once

#include "map.hpp"
#include "sprite2.hpp"
#include "entity.hpp"
#include "scale.hpp"
#include <memory>

class Platform :public ContactInterface, public Entity{
	std::unique_ptr<SingleStringSprite> mSprite;
public:
	Platform(b2World& world,const sf::Vector2u& position,const sf::Vector2u& size,const std::shared_ptr<sf::Texture>& texture,float friction = 1);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void update() override;

	void BeginContact(ContactInterface& entity) override { entity.BeginContact(*this); }
	void EndContact(ContactInterface& entity) override { entity.EndContact(*this); }
	void accept(EntityVisitor& visitor) override{ visitor.visit(*this); }
private:
	void load() override{}
};

struct PlatformFactory :public Map::EntityFactory{
    std::shared_ptr<sf::Texture> texture;
    PlatformFactory(std::shared_ptr<sf::Texture> t): texture(t){

    }
    constexpr char id() const override{ return '#'; }
    constexpr bool canConsolidate() const override{ return true; }
    void make(Map& map,const sf::Vector2u& position,const sf::Vector2u& size) override{
        map.addStaticEntity<Platform>(position + sf::Vector2u{0, 40}, sf::Vector2u{size.x, 10}, texture);
    }
};